Token = {
    get: function() {
	return document.cookie;
    },
    set: function(token) {
	document.cookie = token;
    },
    validate: function(token, callback) {
	$.ajax({
	    type: "GET",
	    url: "/",
	    data: {
		op: "validate_token",
		token: token
	    },
	    success: function(data) {
		var dt = JSON.parse(data);
		callback(dt.ok);
	    }
	});
    }
}