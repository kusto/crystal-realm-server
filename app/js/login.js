function reset_form() {
	$("#login-field").val("");
	$("#password-field").val("");
}

function do_login(login, password) {
	$.ajax({
		type: "GET",
		url: "/",
		data: {
			login: login,
			password: password,
			op: "do_login"
		},
		success: function (data) {
			var dt = JSON.parse(data);
			if (dt.token) {
				Token.validate(dt.token, function (ok) {
					if (ok) {
						Token.set(dt.token);
						location.href = "?token=" + dt.token;
					} else {
						reset_form();
					}
				});
			}
		}
	});
}