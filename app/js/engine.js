/**
 * Created by crystal on 19.05.15.
 */

var from = "";

function render(fen) {
    chess = $("#board").html("").chess({
        fen: fen,
        square_size: 75
    });

    $("div.board_piece").click(function (e) {
        e.preventDefault();
        e.target.style.border = "2px solid black";
        var id = e.target.id;
        id = parseInt(id.replace("board_piece_", ""));
        var x = ("habcdefg")[(id % 8)], y = 8 - Math.floor((id - 1) / 8);
        from += (x + y);
        if (from.length == 4) {
            makeMove(from);
            from = "";
        }
    });
}

function makeMove(move, duration) {
    if (duration === undefined)
        duration = 3000;
    $.ajax({
        url: "?op=chess_api&move=" + move + "&token=" + Token.get(),
        success: function (data) {
            var d = JSON.parse(data);
            if (d.ok == 0) {
                showMessage("Некорректный ход", duration);
            }
            if (d.over) {
                showMessage("Игра закончена<br>" + d.result, -1);
            }
            render(d.fen);
        }
    });
}

function showMessage(message, duration) {
    $("#message").html(message).show();
    if (duration >= 0)
        setTimeout(function () {
            $("#message").hide();
        }, duration);
}
