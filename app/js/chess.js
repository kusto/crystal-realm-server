/*
 * Сделано на основе jChess 0.1.0 (c) 2008 Ben Marini
 */

// Iterate within an arbitrary context...
jQuery.eachWithContext = function (context, object, callback) {
    for (var i = 0, length = object.length, value = object[0];
         i < length && callback.call(context, i, value) !== false; value = object[++i]) {
    }
};

(function ($) {
    /* Constructor */
    $.chess = function (options, wrapper) {
        this.settings = $.extend({}, $.chess.defaults, options);
        this.wrapper = wrapper;

        this.game = {
            active_color: 'w',
            castling_availability: 'KQkq',
            en_passant_square: '-',
            halfmove_clock: 0,
            fullmove_number: 1,
            halfmove_number: 0,

            header: [],
            body: '',
            moves: [],
            annotations: [],
            raw_annotations: [],

            next_piece_id: 64,
            transitions: [],
            board_direction: 1
        };

    };

    /* Add chess() to the jQuery namespace */
    $.fn.chess = function (options) {
        var chess = new $.chess(options, this[0]);
        chess.init();
        return chess;
    };

    $.extend($.chess, {

        defaults: {
            fen: "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
            square_size: 43,
            offsets: {left: 0, top: 0},
            board_element_selector: '.chess-board',
            json_annotations: false
        },

        prototype: {
            init: function () {
                // Load a fresh board position
                this.setUpBoard(this.parseFEN(this.settings.fen));
                this.writeBoard();
            },

            boardElement: function () {
                return $(this.wrapper).find(this.settings.board_element_selector);
            },

            boardData: function () {
                return this._board;
            },

            setUpBoard: function (template) {
                this._board = this.createBoardDataFromTemplate(template);
            },

            createBoardDataFromTemplate: function (template) {
                var board = [];
                $.each(template, function (j, row) {
                    board[j] = [];
                    $.each(row, function (k, val) {
                        if (val != '-') {
                            board[j][k] = {id: (k + 1) + (j * 8), piece: template[j][k].toString()};
                        } else {
                            board[j][k] = "-";
                        }
                    });
                });

                return board;
            },

            writeBoard: function () {
                if (this.boardElement().size() == 0) {
                    $(this.wrapper).append('<div class="chess-board"></div>');
                }

                $.eachWithContext(this, this.boardData(), function (j, row) {
                    $.eachWithContext(this, row, function (k, val) {
                        var piece = this.boardData()[j][k];
                        var square = this.coord2Algebraic(j, k);

                        if (piece != '-') this.addDomPiece(piece.id, piece.piece, square);
                        else this.addDomCell((j * 8) + (k + 1), "", square);
                    });
                });
            },

            getDomPieceId: function (id) {
                return this.wrapper.id + "_piece_" + id;
            },

            addDomPiece: function (id, piece, algebraic) {
                var square = this.algebraic2Coord(algebraic);
                if (this.game.board_direction < 0) {
                    square[0] = 7 - square[0];
                    square[1] = 7 - square[1];
                }

                var pos_top = this.settings.square_size * square[0] + this.settings.offsets.top;
                var pos_left = this.settings.square_size * square[1] + this.settings.offsets.left;

                var color = 'b';
                if (piece.toUpperCase() == piece) {
                    color = 'w';
                }

                this.boardElement().append('<div id="' + this.getDomPieceId(id) + '" class="board_piece ' + color + piece + '"></div>');
                $('#' + this.getDomPieceId(id)).css({position: 'absolute', top: pos_top, left: pos_left});
            },

            addDomCell: function (id, piece, algebraic) {
                var square = this.algebraic2Coord(algebraic);
                if (this.game.board_direction < 0) {
                    square[0] = 7 - square[0];
                    square[1] = 7 - square[1];
                }

                var pos_top = this.settings.square_size * square[0] + this.settings.offsets.top;
                var pos_left = this.settings.square_size * square[1] + this.settings.offsets.left;

                this.boardElement().append('<div id="' + this.getDomPieceId(id) + '" class="board_piece ec"></div>');
                $('#' + this.getDomPieceId(id)).css({position: 'absolute', top: pos_top, left: pos_left});
            },


            moveDomPiece: function (id, move) {
                var from = this.algebraic2Coord(move.from);
                var to = this.algebraic2Coord(move.to);

                var top = (parseInt(to[0]) - parseInt(from[0])) * this.settings.square_size * this.game.board_direction;
                var left = (parseInt(to[1]) - parseInt(from[1])) * this.settings.square_size * this.game.board_direction;

                $('#' + this.getDomPieceId(id)).animate({
                    'top': '+=' + top + 'px', 'left': '+=' + left + 'px'
                }, 'fast');
            },

            removeDomPiece: function (id) {
                $('#' + this.getDomPieceId(id)).remove();
            },

            clearBoard: function () {
                this.boardElement().empty();
            },

            flipBoard: function () {
                var board_length = this.settings.square_size * 7;
                var offsets = this.settings.offsets;

                this.boardElement().children().each(function () {
                    var top_val = parseInt($(this).css('top')) - offsets.top;
                    var left_val = parseInt($(this).css('left')) - offsets.left;
                    $(this).css('top', (board_length - top_val) + offsets.top);
                    $(this).css('left', (board_length - left_val) + offsets.left);
                });

                this.game.board_direction *= -1;
            },

            parseFEN: function (fen) {
                // rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2
                var new_board = [];
                var fen_parts = fen.replace(/^\s*/, "").replace(/\s*$/, "").split(/\/|\s/);

                for (var j = 0; j < 8; j++) {
                    new_board[j] = [];
                    var row = fen_parts[j].replace(/\d/g, this.replaceNumberWithDashes);
                    for (var k = 0; k < 8; k++) {
                        new_board[j][k] = row.substr(k, 1);
                    }
                }
                return new_board;
            },

            validateFEN: function (fen) {
                var pattern = /\s*([rnbqkpRNBQKP12345678]+\/){7}([rnbqkpRNBQKP12345678]+)\s[bw-]\s(([kqKQ]{1,4})|(-))\s(([a-h][1-8])|(-))\s\d+\s\d+\s*/;
                return pattern.test(fen);
            },

            pieceAt: function (algebraic) {
                var square = this.algebraic2Coord(algebraic);
                return this._board[square[0]][square[1]];
            },

            getNextPieceId: function () {
                return ++this.game.next_piece_id;
            },

            /* Utility Functions */
            algebraic2Coord: function (algebraic) {
                return [this.rank2Row(algebraic.substr(1, 1)), this.file2Col(algebraic.substr(0, 1))];
            },

            coord2Algebraic: function (row, col) {
                return this.col2File(col) + this.row2Rank(row);
            },

            rank2Row: function (rank) {
                return 8 - parseInt(rank);
            },

            file2Col: function (file) {
                return file.charCodeAt(0) - ('a').charCodeAt(0);
            },

            row2Rank: function (row) {
                return (8 - row) + '';
            },

            col2File: function (col) {
                return String.fromCharCode(col + ('a').charCodeAt(0));
            },
            replaceNumberWithDashes: function (str) {
                var num_spaces = parseInt(str);
                var new_str = '';
                for (var i = 0; i < num_spaces; i++) {
                    new_str += '-';
                }
                return new_str;
            },

            /* Patterns used for parsing */
            patterns: {
                castle_kingside: /^O-O/,
                castle_queenside: /^O-O-O/,

                piece_move: /^([BKNQR])/,
                rank_and_file_given: /^([BKNQR])([a-h])([1-8])x?([a-h])([1-8])/,
                file_given: /^([BKNQR])([a-h])x?([a-h])([1-8])/,
                rank_given: /^([BKNQR])([1-8])x?([a-h])([1-8])/,
                nothing_given: /^([BKNQR])x?([a-h])([1-8])/,

                pawn_move: /^([a-h])([1-8])/,
                pawn_capture: /^([a-h])x([a-h])([1-8])/,
                pawn_queen: /=([BNQR])/
            },

            /* Definitions of pieces */
            pieces: {
                R: {
                    vectors: [
                        {x: 0, y: 1, limit: 8},
                        {x: 1, y: 0, limit: 8},
                        {x: 0, y: -1, limit: 8},
                        {x: -1, y: 0, limit: 8}
                    ]
                },
                N: {
                    vectors: [
                        {x: 1, y: 2, limit: 1},
                        {x: 2, y: 1, limit: 1},
                        {x: 2, y: -1, limit: 1},
                        {x: 1, y: -2, limit: 1},
                        {x: -1, y: -2, limit: 1},
                        {x: -2, y: -1, limit: 1},
                        {x: -2, y: 1, limit: 1},
                        {x: -1, y: 2, limit: 1}
                    ]
                },
                B: {
                    vectors: [
                        {x: 1, y: 1, limit: 8},
                        {x: 1, y: -1, limit: 8},
                        {x: -1, y: -1, limit: 8},
                        {x: -1, y: 1, limit: 8}
                    ]
                },
                Q: {
                    vectors: [
                        {x: 0, y: 1, limit: 8},
                        {x: 1, y: 0, limit: 8},
                        {x: 0, y: -1, limit: 8},
                        {x: -1, y: 0, limit: 8},

                        {x: 1, y: 1, limit: 8},
                        {x: 1, y: -1, limit: 8},
                        {x: -1, y: -1, limit: 8},
                        {x: -1, y: 1, limit: 8}
                    ]
                },
                K: {
                    vectors: [
                        {x: 0, y: 1, limit: 1},
                        {x: 1, y: 0, limit: 1},
                        {x: 0, y: -1, limit: 1},
                        {x: -1, y: 0, limit: 1},

                        {x: 1, y: 1, limit: 1},
                        {x: 1, y: -1, limit: 1},
                        {x: -1, y: -1, limit: 1},
                        {x: -1, y: 1, limit: 1}
                    ]
                }
            }
        }
    });
})(jQuery);