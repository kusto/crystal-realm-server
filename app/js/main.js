if (window && window.localStorage && window.localStorage.getItem('CONFIG'))
    CONFIG = JSON.parse(window.localStorage.getItem('CONFIG'));
else
    CONFIG = {
        show_kill: false,
        show_coord: false,
        show_status: false,
        show_effects: false,
        show_hp: true,
        show_mp: true
    };

function leadingZero(num, len) {
    var s = num + "";
    while (s.length < len) s = "0" + s;
    return s;
}

function thread(thr, status) {
    return "<span class='" + (status ? 'ok' : 'fail') + "'>" + thr + "</span>";
}

function threads(json) {
    var js = JSON.parse(json);
    var string = "";
    var allok = true;
    for (var key in js) {
        string += thread(key, js[key]);
        if (!js[key]) allok = false;
    }

    $("aside.threads").html(string);

    $("#show-status").html(allok ? "✔" : "⚠");

}

function cpu(data) {
    var dt = JSON.parse(data);
    var string = "";
    for (var i in dt.cpu_cores) {
        string += "<progress class='cpumeter' value='" + dt.cpu_cores[i] + "' max='100'></progress>";
    }
    $("#cpu").html(string);
    string = "<progress class='memmeter' value='" + dt.mem + "' max='100'></progress>";
    $("#mem").html(string);
    console.log(string);
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToARGB(i) {
    return ((i >> 16) & 0xFF).toString(16) +
        ((i >> 8) & 0xFF).toString(16) +
        (i & 0xFF).toString(16);
}

function wrap(str) {
    return "<div class='block'>" + str + "</div>";
}

function gettype(type) {
    if (type == 1) return "игрок";
    if (type == 2) return "NPC";
    if (type == 3) return "враг";
    if (type == 4) return "няшка";
}

function getpos(pos) {
    //console.log(pos);
    if (CONFIG.show_coord)
        return "(" + pos.x + ";" + pos.y + ";" + pos.z + ")";
    else
        return "";
}

function getstate(state) {
    if (CONFIG.show_status) {
        if (state.state == 0) return "Жив";
        if (state.state == 1) return "Мертв";
        if (state.state == 2) return "Воскрешение";
        if (state.state == 3) return "АГРО";
        if (state.state == 4) return "ОФФЛАЙН";
        return state.state;
    } else return "";
}

function geteffect(e) {
    return "<div class='effect' title='" + e.id + "' style='background-color: #" + intToARGB(hashCode(e.id)) + ";'> </div>";
}

function geteffects(eff) {
    var string = "";
    for (var e in eff)
        string += geteffect(eff[e]);

    return string;
}

function json2table(obj) {
    if (obj.type == 5) return "";
    var str = "<table>";
    str += "<tr><td>[ " + gettype(obj.type) + " ]</td><td><b><a href='#' onclick=\"insertAtCaret('code','ID(\\'" + obj.id + "\\')'); return false;\">" + obj.id + "</b></td></tr>";
    if (CONFIG.show_status || CONFIG.show_coord)
        if (obj['pos'] !== undefined) str += "<tr><td>" + getpos(obj.pos) + "</td><td>" + getstate(obj.state) + "</td></tr>";
    if (CONFIG.show_hp)
        if (obj['hp'] !== undefined) str += "<tr><td>HP</td><td><progress class='hpbar' value='" + obj.hp + "' max='" + obj.max_hp + "'></progress></td></tr>";
    if (CONFIG.show_mp)
        if (obj['mp'] !== undefined) str += "<tr><td>MP</td><td><progress class='mpbar' value='" + obj.mp + "' max='" + obj.max_mp + "'></progress></td></tr>";
    if (CONFIG.show_effects)
        str += "<!--suppress HtmlUnknownAttribute --><tr height='20px'><td colspan='2'>" + geteffects(obj.effects) + "</td></tr>";

    if (CONFIG.show_kill)
        str += "<tr><td colspan='2'><a href='#' onclick='return kill(\"" + obj.id + "\");'>Убить</a></td></tr>";

    //for(var i in obj) str += "<tr><td>"+i+"</td><td>"+obj[i]+"</td></tr>";
    str += "</table>";
    return wrap(str);
}

function load(json) {
    var d = JSON.parse(json);
    //var d = json;
    d = d.data;
    var str = "";
    for (var i in d) {
        str += json2table(d[i]);
    }
    //  console.log(json);
    $("#container").html(str);
}

function reload() {
    $.ajax({
        success: load,
        url: "?op=list",
        type: "GET",
        data: {
            token: Token.get()
        }
    });
    $.ajax({
        success: threads,
        url: "?op=threads",
        type: "GET", data: {
            token: Token.get()
        }
    });
}

function kill(id) {
    $.ajax({
        success: reload,
        url: "/",
        type: "GET",
        data: {
            op: "kill",
            id: id,
            token: Token.get()
        }
    });
    return false;
}

function eval() {
    $.ajax({
        success: function (data) {
            $("#console").prepend("<div>" + data + "</div>")
        },
        url: "/",
        data: {
            op: "eval",
            code: $("#code").val(),
            token: Token.get()
        },
        type: "GET"
    });
}

function shutdown() {
    $.ajax({
        url: "/",
        type: "GET",
        data: {
            op: "shutdown",
            token: Token.get()
        }
    });
}

function getTime() {
    $.ajax({
        success: function (data) {
            var d = JSON.parse(data);

            var hour = Math.floor(d.h);
            d.h -= hour;
            d.h *= 60;
            var minute = Math.floor(d.h);

            $("#time").html(leadingZero(hour, 2) + " : " + leadingZero(minute, 2));
        },
        data: {
            op: "time",
            token: Token.get()
        },
        url: "/",
        type: "GET"
    });

    $.ajax({
        success: cpu,
        data: {
            op: "cpu",
            token: Token.get()
        },
        url: "?op=cpu",
        type: "GET"
    });
}


(function ($) {
    $.fn.onEnter = function (func) {
        this.bind('keypress', function (e) {
            if (e.keyCode == 13) func.apply(this, [e]);
        });
        return this;
    };
})(jQuery);


function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var range;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") {
        txtarea.focus();
        range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        range.moveStart('character', strPos);
        range.moveEnd('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}
