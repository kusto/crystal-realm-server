"""
@ToDo find quote 4 this class
"""
import core.demeter.singleton
from core.demeter.generic_server import GenericWorker
# noinspection PyUnresolvedReferences
from .btcSPhysics import SPhysics as btc


class Persephone(GenericWorker, metaclass=core.demeter.singleton.Singleton):
    id = {}
    big = 1000000
    server_id = "persephone"
    server_name = "Persephone (шизика)"

    def start(self):
        pass

    def stop(self):
        self.exit()

    def __init__(self):
        super().__init__()
        self.inst = None

    def load(self):
        self.inst = btc.sin()
        self.inst.init_physics()

    def step(self, lst):
        self.inst.step_simulation()
        self.inst.syncronize(lst)
        # self.process_events(list)

    def exit(self):
        self.inst.exit_physics()

    def add(self, character):
        self.inst.add(character)
        self.id[self.big * character.type + character.id] = character.gameid

    def remove(self, character):
        self.inst.remove(character)

    def on(self, character):
        self.inst.activate(character)

    def off(self, character):
        self.inst.deactivate(character)

    def goto(self, character, x, y, z):
        character.position.x, character.position.y, character.position.z = x, y, z
        character.position.to_x, character.position.to_y, character.position.to_z = x, y, z
        self.inst.p_set_position(character.type, character.id, x, y, z)

    def dist(self, character1, character2):
        return self.inst.distance(character1.position.x, character1.position.y, character1.position.z,
                                  character2.position.x, character2.position.y, character2.position.z)

    def process_events(self, chars):
        event = self.inst.get_event()
        while (event[2] != event[3]):
            enemy = chars[self.id[3 * self.big + event[0]]]
            player = chars[self.id[self.big + event[1]]]
            if (event[3] < 3):
                enemy.position.to_x, enemy.position.to_y, enemy.position.to_z = player.position.x, player.position.y, player.position.z
            elif (event[3] == 3):
                enemy.position.to_x, enemy.position.to_y, enemy.position.to_z = enemy.position.x, enemy.position.y, enemy.position.z
            event = self.inst.get_event()