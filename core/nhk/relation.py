__author__ = 'crystal'

from .basemodel import BaseModel
from .talk import TalkDB
from peewee import *


class RelationDB(BaseModel):
    npc = CharField(unique=True)
    talk = ForeignKeyField(db_column='talk', rel_model=TalkDB, to_field='gameid')

    class Meta:
        db_table = 'nhk_relations'

