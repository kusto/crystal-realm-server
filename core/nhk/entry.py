__author__ = 'crystal'

# noinspection PyUnresolvedReferences
import logging


# noinspection PyBroadException
class Entry:
    def __init__(self, talk, character):
        self.talk = talk
        self.state = talk.init_state
        self.character = character
        self.options = self.talk.states[self.state].get_options(self.character)
        self.finished = 0

    def process(self, data):
        """
        Тут происходит все основное веселье
        Эта функция отвечает за общение с игроком, если можно так выразиться
        """
        try:
            if 'option' not in data:
                return self.dump()

            if data['option'] not in self.options:
                return self.dump()

            self.talk.states[self.state].siblings[data['option']].passed(self.character)
            self.state = data['option']
            self.finished = self.talk.states[self.state].terminal
            self.options = self.talk.states[self.state].get_options(self.character)
            return self.dump()

        except:
            return self.dump()

    def dump(self):
        return {
            "type": "nhk_talk",
            "talk": self.talk.id,
            "state": self.state,
            "options": self.options,
            "finished": self.finished
        }