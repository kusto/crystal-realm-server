__author__ = 'crystal'

from .basemodel import BaseModel
# noinspection PyUnresolvedReferences
from .state import StateDB
from peewee import *
import json
from core.athena.athena import Athena
from core.hecate.hecate import Hecate


# noinspection PyBroadException,PyBroadException
class Pass:
    def __init__(self, model):
        self.parent = model.parent
        self.sibling = model.sibling
        self.talk = model.talk
        try:
            self.action = json.loads(model.action)
        except:
            self.action = {}

        try:
            self.requirement = json.loads(model.requirement)
        except:
            self.requirement = {}

    def can(self, character):
        if 'level' in self.requirement and character.level < self.requirement['level']:
            return False

        if 'logic' in self.requirement and not Athena().evaluate(character, self.requirement['logic']):
            return False

        return True

    def passed(self, character):
        if 'quest' in self.action:
            for key in self.action['quest']:
                Hecate().set_state(character, key, self.action['quest'][key])


class PassDB(BaseModel):
    action = TextField()
    parent = IntegerField()
    requirement = TextField()
    sibling = IntegerField()
    talk = IntegerField()

    class Meta:
        db_table = 'nhk_pass'
