__author__ = 'crystal'

from .basemodel import BaseModel
from peewee import *


class Talk:
    def __init__(self, model):
        self.id = model.id
        self.gameid = model.gameid
        self.states = {}

        self.cls = ""

        self.init_state = model.init_state

    def get_state(self, state):
        if state in self.states:
            return self.states[state]
        raise KeyError(state)


class TalkDB(BaseModel):
    id = IntegerField()
    gameid = CharField(unique=True)
    init_state = IntegerField()

    class Meta:
        db_table = 'nhk_talk'

