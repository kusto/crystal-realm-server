__author__ = 'crystal'

from .basemodel import BaseModel
from peewee import *
# noinspection PyUnresolvedReferences
from .talk import TalkDB


class State:
    def __init__(self, model):
        self.state = model.state
        self.talk = model.talk
        self.terminal = model.terminal
        self.siblings = {}

    def get_options(self, character):
        return [key for key in self.siblings if self.siblings[key].can(character)]


class StateDB(BaseModel):
    state = IntegerField()
    talk = IntegerField()
    terminal = IntegerField()

    class Meta:
        db_table = 'nhk_state'