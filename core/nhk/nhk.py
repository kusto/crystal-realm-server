__author__ = 'crystal'

"""
Ababa ababa ababa odoru akachan ningen

Hito wa hadaka de umareta toki wa daremo aisare onaji na hazu ga
Doushite nanoda ikiteiku uchi
Sadame wa wakare mugoi kurai da

Hito no me mitari mirenakattari koi wo shittari shirenakattari
Sorenara boku wa issonaritai shinu made baby akachan ningen

"A~nante kawaii baby aishitaku naruwa"
(Omou tsubodaze) (Umakuittaze)

Kanojo daita akago jitsu wa akachan ningen
Adokenasa no urade ababa hokusi emu nosa
Kimi mo nareyo rakude iize baby human
Ababa ababa odore fuyase sekai seihada
Roshia wo kanada wo indo mo

Omou dake nara ousama nano ni mitsumeteireba koibito na noni
Doushite nanoda genjitsu nanda shinjitsu de sae hitsuyou nanoka

Warai sazameku furishite mitemo muri ga aruneto iwareta hi niwa
Boku wa naru no sa sore shikanaize shinumate baby akachan ningen

"A~nante kashikoi baby aishitakunaruwa"
(Sou demo naize) (Konna mondaze)

Kanojo daita akago nanto akachan ningen
Kakuremi nowa akai obebe kamofurajuusa
Baito shigoto naize iize baby human
Ababa ababa odore fuyase chikyuu kono te ni
Chaina wo yuuro wo toruko mo

"A~nante kawaii baby aishitaku naruwa"
(Omou tsubodaze) (Umakuittaze)

Kanojo daita akago jitsu wa akachan ningen
Adokenasa no urade ababa hokuso emu nosa
Kimi mo nareyo rakude iize baby human
Ababa ababa odore fuyase soshite uchuu e

Kasei e dosei e ginga e
Odoru yo akachan ningen
Odoru yo akachan ningen
"""

from . import talk, state, passage, entry, relation
from core.cronus.cronus import Cronus
from core.ariadne.ariadne import Ariadne
from core.demeter.singleton import Singleton
from core.demeter.i18n import I18n


# noinspection PyBroadException
class NHK(metaclass=Singleton):
    def __init__(self):
        self.talks = {}
        self.relations = {}
        self.cronus = Cronus()

    def load(self):
        """
        Работать напрямую с базой - это не пусть сильного человека.
        Поэтому сейчас мы будем кешировать лес.
        Надеюсь, никто не решит воспользоватьсья этой реализацией для своего проекта
        Серьезно, не стоит даже и пытаться, лучше написать саоммстоятельно
        """

        self.buildCache_forestRoots()
        self.buildCache_treeNodes()
        self.buildCache_treeBranches()

        """
        Кеш отношений NPC - разговор (если быть совсем точным - то класс NPC - разговор)
        И да, здесь я тоже воткнлу внешние ключи в БД чтобы обойтись без лишних проверок
        """
        self.buildCache_relations()

        Ariadne().bind("nhk_talk", self.bind)

    def buildCache_forestRoots(self):
        talks = talk.TalkDB.select()
        for single in talks:
            obj = talk.Talk(single)
            self.talks[obj.id] = obj

    def buildCache_treeNodes(self):
        states = state.StateDB.select()
        for single in states:
            obj = state.State(single)
            """
            Мы не проверяем тут наличие такого talk-а потому, что поле talk в MySQL указано как внешний ключ
            Вы вряд-ли захотите переносить такое решение на продакшн
            """
            self.talks[obj.talk].states[obj.state] = obj

    def buildCache_treeBranches(self):
        passes = passage.PassDB.select()
        for single in passes:
            obj = passage.Pass(single)
            """
            Не буду повторяться по поводу причин, почему мы забиваем на проверки.
            """
            self.talks[obj.talk].states[obj.parent].siblings[obj.sibling] = obj

    def buildCache_relations(self):
        relations = relation.RelationDB.select()
        for single in relations:
            self.relations[single.npc] = self.talks[single.talk.id]

    def bind(self, data):
        char = self.cronus.objects[data['session']['character_id']]

        if not char.nhk_talk:
            try:
                tlk = self.relations[
                    self.cronus.classes[
                        self.cronus.objects[
                            data['npc']
                        ].classid
                    ].gameid
                ]
                char.nhk_talk = entry.Entry(tlk, char)
                char.nhk_talk.cls = self.cronus.classes[
                    self.cronus.objects[
                        data['npc']
                    ].classid
                ].gameid
            except:
                # noinspection PyProtectedMember
                return I18n()._("nhk/reject")

        o = char.nhk_talk.process(data)
        if o['finished']:
            char.nhk_talk = None

        return o