__author__ = 'crystal'

from . import skill
# import logging


class SkillEffect(skill.Skill):
    def __init__(self, model, ares=None):
        super().__init__(model, ares)
        param = model.skill_parse_param()
        self.effect = param['effect']

    def use(self, caster, target):
        super().use(caster, target)
        if self.ares.cast_effect(caster, target, self.ares.effects[self.effect]):
            pass

    def dump(self):
        t = super().dump()
        t['effect'] = self.effect
        return t

    def is_usable(self, data, spec):
        if not super(SkillEffect, self).is_usable(data, spec):
            return False

        if spec['caster'].is_friendly(spec['target']):
            return False

        return True