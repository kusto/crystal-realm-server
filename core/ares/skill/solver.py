__author__ = 'crystal'

from . import skill
from core.hermes.hermes import Hermes
# import logging

"""
Ничего не даю. Никого не благославляю. Никого не спасаю. Только стираю. Полностью. В пыль.
"""


class SkillSolver(skill.Skill):
    def __init__(self, model, ares=None):
        super().__init__(model, ares)
        param = model.skill_parse_param()
        self.damage = param['damage']
        self.typ = param['type'] if 'type' in param else 'clear'

    def use(self, caster, target):
        super().use(caster, target)
        if self.ares.deal_damage(caster, target, self.damage, self.typ):
            pass

        if target.type == 1 and target.client:
            Hermes().notify_single({"event": "system_message", "msg": "Mene, Mene, Tekel, Upharsin"}, target.client)

    def is_usable(self, data, spec):
        if not super(SkillSolver, self).is_usable(data, spec):
            return False

        if spec['caster'].is_friendly(spec['target']):
            return False

        return True

    def dump(self):
        t = super().dump()
        t['damage'] = self.damage
        return t