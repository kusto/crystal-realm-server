__author__ = 'crystal'

from peewee import *
from . import basemodel


class SkillPlayerRelation:
    @staticmethod
    def get_skills(playerid):
        skills = SkillPlayerRelationDB.select().where(SkillPlayerRelationDB.playerid == playerid)
        summ = {}
        for skill in skills:
            summ[skill.skillid] = {
                'last': 0
            }

        return summ

    @staticmethod
    def add_skill(player, skill):
        # noinspection PyPep8
        skills = SkillPlayerRelationDB \
            .select() \
            .where \
            (
                (SkillPlayerRelationDB.playerid == player) &
                (SkillPlayerRelationDB.skillid == skill)
            ) \
            .limit(1)

        # noinspection PyUnusedLocal
        for skill in skills:
            return False

        s = SkillPlayerRelationDB()
        s.playerid = player
        s.skillid = skill
        s.save()

        return True


class SkillPlayerRelationDB(basemodel.BaseModel):
    playerid = CharField()
    skillid = IntegerField()

    class Meta:
        db_table = 'skill_player_relation'
