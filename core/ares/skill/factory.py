__author__ = 'crystal'

from .skill import Skill
# noinspection PyUnresolvedReferences
from . import *


class Factory:
    def __init__(self, ares):
        self.skills = {}
        self.ares = ares
        for cls in Skill.__subclasses__():
            self.skills[cls.__name__] = cls

    def get_class(self, model):
        cname = "Skill"+model.class_[0].upper()+model.class_[1:]
        if cname in self.skills.keys():
            return self.skills[cname](model, ares=self.ares)
        else:
            return None