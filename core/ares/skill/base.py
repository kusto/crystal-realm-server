__author__ = 'crystal'

from . import skill
# import logging


class SkillBase(skill.Skill):
    def __init__(self, model, ares=None):
        super().__init__(model, ares)
        param = model.skill_parse_param()
        self.damage = param['damage']
        self.typ = param['type'] if 'type' in param else 'clear'

    def use(self, caster, target):
        super().use(caster, target)
        if self.ares.deal_damage(caster, target, self.damage, self.typ):
            pass

    def is_usable(self, data, spec):
        if not super(SkillBase, self).is_usable(data, spec):
            return False

        if spec['caster'].is_friendly(spec['target']):
            return False

        return True

    def dump(self):
        t = super().dump()
        t['damage'] = self.damage
        return t