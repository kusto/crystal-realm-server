__author__ = 'crystal'

from . import basemodel
from peewee import *
import json


# noinspection PyBroadException
class SkillDB(basemodel.BaseModel):
    id = IntegerField()
    class_ = CharField(db_column='class')
    gameid = CharField()
    name = CharField()
    param = TextField()
    cooldown = IntegerField()
    cost = IntegerField()
    max_range = IntegerField()

    class Meta:
        db_table = 'skill'

    def skill_parse_param(self):
        try:
            return json.loads(self.param)
        except Exception:
            return {}
