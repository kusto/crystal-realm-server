__author__ = 'crystal'

import logging
from time import time
from core.demeter.hook_manager import HookManager
from physics.persephone import Persephone


class Skill:
    def __init__(self, model, ares=None):
        self.ares = ares

        self.id = model.id
        self.gameid = model.gameid
        self.name = model.name
        self.cd = model.cooldown
        self.cost = model.cost
        self.range = model.max_range

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)
        self.logger.debug("ares/skill/skill/loaded", {'skill': self.gameid})

    def is_usable(self, data, spec):
        if data['last'] + self.cd > time():
            return False

        if not spec['target'].is_interactable():
            return False

        if not spec['caster'].state.alive():
            return False

        if not spec['target'].state.alive():
            return False

        if spec['caster'].mp < self.cost:
            return False

        if 0 <= self.range < Persephone().dist(spec['target'], spec['caster']):
            return False

        return True

    def use(self, source, target):
        # self.logger.debug("ares/skill/skill/casted", {'skill': self.gameid})
        source.mp -= self.cost
        HookManager().execute_event("ares/skill/cast", source=source, target=target, skill=self)

    def dump(self):
        return {
            'cooldown': self.cd
        }