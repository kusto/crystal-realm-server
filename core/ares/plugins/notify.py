__author__ = 'crystal'

import core.hermes.hermes
from core.demeter.hook_manager import HookManager


# noinspection PyUnusedLocal
class Notify:
    def __init__(self):
        self.hermes = core.hermes.hermes.Hermes()
        HookManager().register_hook("ares/skill/cast", "ares/plugin/notify", self.cast)
        HookManager().register_hook("cronus/state-change", "ares/plugin/notify", self.state_change)
        # HookManager().register_hook("cronus/remove", "cronus/plugin/hermes", self.remove)
        # HookManager().register_hook("cronus/mendel", "cronus/plugin/hermes", self.mendel)

    def cast(self, source=None, target=None, skill=None):
        nb = {
            "event": "arest_cast",
            "caster": source.gameid,
            "target": target.gameid,
            "skill": skill.gameid
        }

        self.hermes.notify(nb, target.square)

        self.state_change(source)
        self.state_change(target)

    def state_change(self, obj=None):
        nb = {
            "event": "hermes_cronus_state_change",
            "data": obj.dump()
        }

        self.hermes.notify(nb, obj.square)
        if obj.type == 1 and obj.group:
            self.hermes.notify(nb, obj.group)
