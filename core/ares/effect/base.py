__author__ = 'crystal'

# noinspection PyUnresolvedReferences
import time
from . import effect


class EffectBase(effect.Effect):
    def __init__(self, model, ares=None):
        super().__init__(model, ares)
        param = model.effect_parse_param()
        self.duration = param['duration']
        self.skill = param['skill']
        self.period = param['period']
        self.ares = ares

    def step(self, param, tick):
        if tick - param['last'] > self.period:
            self.ares.skills[self.skill].use(param['caster'], param['target'])
            param['last'] += self.period

        if tick - param['first'] > self.duration:
            return True

        return False

    def dump(self):
        return {
            'id': self.gameid,
            'duration': self.duration
        }