__author__ = 'crystal'

import logging


class Effect:
    def __init__(self, model, ares=None):
        self.ares = ares

        self.id = model.id
        self.gameid = model.gameid
        self.name = model.name

        logging.getLogger(__name__ + '.' + self.__class__.__name__) \
            .debug("ares/effect/effect/effect-loaded", {'effect': model.gameid})

    def step(self, param, tick):
        pass

    def on_cast(self):
        pass

    def on_remove(self):
        pass

    def dump(self):
        pass