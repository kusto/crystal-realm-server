__author__ = 'crystal'

from .effect import Effect
# noinspection PyUnresolvedReferences
from . import *


class Factory:
    def __init__(self, ares):
        self.effects = {}
        self.ares = ares
        for cls in Effect.__subclasses__():
            self.effects[cls.__name__] = cls

    def get_class(self, model):
        cname = "Effect"+model.class_[0].upper()+model.class_[1:]
        if cname in self.effects.keys():
            return self.effects[cname](model, ares=self.ares)
        else:
            return None