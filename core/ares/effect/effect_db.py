__author__ = 'crystal'

from . import basemodel
from peewee import *
import json


# noinspection PyBroadException
class EffectDB(basemodel.BaseModel):
    id = IntegerField()
    class_ = CharField(db_column='class')
    gameid = CharField()
    name = CharField()
    param = TextField()

    class Meta:
        db_table = 'effect'

    def effect_parse_param(self):
        try:
            return json.loads(self.param)
        except Exception:
            return {}
