__author__ = 'crystal'
"""
Ничто так не потешит бога войн,
Как битва славная! И вот во имя бога,
Под стенами у замка собрались
Отряды воинов! И ждут сигнала рога.

В лесу древнейшем замок тот стоял,
И тишина была его уделом.
И только ветер тишину ту нарушал,
Да гость нечастый там бродил несмело.

И вот однажды там сгустилась мгла.
Все замерло в предчувствии тревожном.
И вечером осада началась,
Отряды замок окружили осторожно.
"""

# noinspection PyUnresolvedReferences
import logging
import time
# noinspection PyUnresolvedReferences
import copy
from core.demeter.decorator import *
from core.demeter.singleton import Singleton
from .skill import skill_db
from .skill import skill_player
from .skill import factory as skill_factory
from .effect import effect_db
from .effect import factory as effect_factory
from .plugins import *
from core.hermes.hermes import Hermes
from core.ariadne.ariadne import Ariadne
from core.demeter.hook_manager import HookManager


# noinspection PyBroadException
class Ares(metaclass=Singleton):
    def __init__(self):
        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.cronus = None

        # ID -> GameID
        self.skills_lookup = {}

        # GameID -> ID
        self.skills_lookup_reverse = {}

        # GameID -> Skill
        self.skills = {}

        # PlayerID (int) -> { GameID -> SkillEntry }
        self.skills_p_cache = {}

        """
        Слава всем богам, что в эффектах непришлось делать настолько же сложную структуру.
        """

        self.effects = {}

    def load(self):
        fact = skill_factory.Factory(self)
        skills = skill_db.SkillDB.select()
        for c in skills:
            cls = fact.get_class(c)
            if cls:
                self.skills[c.gameid] = cls
                self.skills_lookup[c.id] = c.gameid
                self.skills_lookup_reverse[c.gameid] = c.id
            else:
                self.logger.debug("ares/ares/skill-load-error", {'skill': c.gameid})

        fact = effect_factory.Factory(self)
        effects = effect_db.EffectDB.select()
        for c in effects:
            cls = fact.get_class(c)
            if cls:
                self.effects[c.gameid] = cls
            else:
                self.logger.debug("ares/ares/effect-load-error", {'effect': c.gameid})

        Ariadne().bind("ares_skill_list", self.get_skills)
        Ariadne().bind("ares_skill", self.cast_skill)
        Ariadne().bind("ares_pk", self.switch_pk_mode)

        HookManager().register_hook("cronus/character/step", "ares/process_effects", self.process_effects)

        notify.Notify()

    def skills_rebuild_cache_row(self, pid):
        try:
            row = skill_player.SkillPlayerRelation.get_skills(pid)
            self.skills_p_cache[pid] = {self.skills_lookup[key]: row[key] for key in row}
        except:
            self.logger.exception("ares/ares/skill-cache-error")

    def skills_clear_cache_row(self, pid):
        if pid in self.skills_p_cache:
            del (self.skills_p_cache[pid])

    def skills_get_cache_row(self, pid):
        return self.skills_p_cache[pid]

    def skills_get_cache_session(self, data):
        return self.skills_p_cache[data['session']['character_id']]

    def get_skills(self, data):
        try:
            """
            Почему я использую недокументированные поля ? - спросите вы.
            Хм, я даже затрудняюсь определить причину.
            Может это потому, что уважение к недокументировоанным особенностям у меня отбило WinAPI
            Может потому, что я сам написал это поле и я сам же и знаю, что оно никуда не денется.
            Может потому, что оно просто работает.
            Это, как бы, не наибольший из моих грехов. Там по соседству используются служебные переменные класса.
            """
            cid = data['session']['character_id']

            """
            На самом деле, не совсем очевидное решение.
            Его корни кроются в том, что запрос на список навыков приходиот от клиента при логине.
            Таким образом, большой нагрузки на базу перестроение кеша не даст.
            С другой стороны, это может избавить от тонны проблем, связанных с устареванием кеша.
            """
            self.skills_rebuild_cache_row(cid)

            return {
                'data': {key: self.skills[key].dump() for key in self.skills_p_cache[cid]}
            }
        except:
            self.logger.exception("ares/ares/skill-cache-error")
            return None

    @deprecated
    def _get_skills(self, cid):
        return self.skills_rebuild_cache_row(cid)

    def add_skill(self, player, skill):
        if skill in self.skills_lookup_reverse:
            skill = self.skills_lookup_reverse[skill]

        skill_player.SkillPlayerRelation.add_skill(player.gameid, skill)

        Hermes().notify_single({"event": "ares_skill_add", "skill": skill}, player.client)

        self.skills_rebuild_cache_row(player)

    def cast_skill(self, data):
        if 'skill' not in data:
            self.logger.debug("general/broken-packet")
            return None

        try:
            row = self.skills_get_cache_session(data)

            if data['skill'] not in row:
                self.logger.debug("ares/ares/injection-rejection")
                return None

            if data['skill'] not in self.skills:
                self.logger.debug("ares/ares/skill-not-loaded", {'skill': data['skill']})
                return None

            ent = row[data['skill']]

            datagram = copy.copy(data['data'])
            datagram['caster'] = self.cronus.objects[data['session']['character_id']]
            datagram['target'] = self.cronus.objects[datagram['target']]

            # ToDo: заменить skill.use(caster, target) на skill.use_dg(datagram) (как ниже)
            # self.skills[data['skill']].use_dg(datagram)

            try:
                self.cronus.objects[data['session']['character_id']].on_activity()
            except:
                pass

            if self.skills[data['skill']].is_usable(ent, datagram):
                self.skills[data['skill']].use(
                    datagram['caster'],
                    datagram['target'],
                )

                ent['last'] = time.time()

                df = copy.copy(data['data'])
                df['caster'] = datagram['caster'].gameid
                df['target'] = datagram['target'].gameid
                df['skill'] = data['skill']
            else:
                # noinspection PyProtectedMember
                return I18n()._("ares/ares/bad-target")

        except:
            self.logger.exception("general/error")
            return None

    # noinspection PyMethodMayBeStatic
    def deal_damage(self, dd, tank, damage, typ="clear"):
        if not tank.state.alive():
            return None

        damage = dd.stats.get_real_damage_sent(damage, typ)
        damage = tank.stats.get_real_damage_got(damage, typ)

        tank.hp -= damage

        if tank.hp < 0:
            tank.hp = 0

        tank.on_damage(dd)
        dd.on_damage_sent(tank)

        # HookManager().execute_event("cronus/state-change", obj=tank)
        return True

    def cast_effect(self, caster, target, effect):
        if not target.state.alive():
            return None
        tm = self.cronus.current_tick
        casted_effect = {
            'effect': effect,
            'caster': caster,
            'target': target,
            'last': tm,
            'first': tm
        }
        target.effects.append(casted_effect)
        return True

    def switch_pk_mode(self, data):
        if 'value' not in data:
            value = 0
        else:
            value = data['value']

        try:
            self.cronus.objects[data['session']['character_id']].pk_mode = value
            # self.logger.debug("Переключен режим убийцы в "+value+" для "+data['session']['character_id'])
        except:
            self.logger.exception("general/error")

    def process_effects(self, character=None):
        tick = self.cronus.current_tick
        if not character:
            return False

        try:
            if not character.effects:
                return False
        except:
            self.logger.exception("general/error")
            return False

        delcache = []
        for key, e in enumerate(character.effects):
            if character.effects[key]['effect'].step(character.effects[key], tick):
                delcache.append(key)

        for key in delcache:
            del (character.effects[key])
