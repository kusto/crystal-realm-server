__author__ = 'crystal'

from core.demeter.singleton import Singleton
from core.pandora.pandora import Pandora
from playhouse.pool import PooledMySQLDatabase, PooledDatabase
from core.demeter.generic_server import GenericServer

import heapq
import time
import logging

"""
Основано на PeeWee Playhouse PooledMySQLDatabase

На самом деле, PeeWee - очень хорошая ORM, но вот с исправлением потерь соединения я мучался очень долго

Станндартно предлагается вариант с пингом соединения через некоторое время.
В реальности возникала проблема, состоящая в том, что при работе сервера превшался не только idle, но и
interactive таймаут и соединение рвалось.
Кроме того, сам MySQL тоже частенько зависал

После этого я пришел к решению с пулом.
Это решение было весьма неплохим, за вычетом того, что к треду прибивалось соединение.
Поскольку не все треды сервера создаются и быстро умирают, а некоторые напротив живут долго, то в итоге
проблема повторилась в точности.

Итоговое решение было основано на пуле, но работает по-другому.
Оно проверяет соединения и отдает новое каждый раз, когда оно нужно.
Это позволяет держать все (ну или почти все) соединения более-менее живыми.
Решение было протестировоано с таймаутом MySQL 80 секунд и нормальтно работало.
На всякий случай, однако, соединения все еще перепроверяются раз в 10 секунд
"""


# noinspection PyAbstractClass
class PooledMagicMySQLDatabase(PooledMySQLDatabase):
    # noinspection PyUnusedLocal
    def _connect(self, *args, **kwargs):
        while True:
            try:
                ts, conn = self._connections[0]
                key = self.conn_key(conn)
            except IndexError:
                ts = conn = None
                break
            else:
                if self.stale_timeout and self._is_stale(ts):
                    self._close(conn, True)
                    ts = conn = None
                    heapq.heappop(self._connections)
                    logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                        .debug("uranus/close-connection")
                elif self._is_closed(key, conn):
                    ts = conn = None
                    heapq.heappop(self._connections)
                    self._closed.discard(key)
                    logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                        .debug("uranus/close-connection")
                else:
                    break

        # noinspection PyUnboundLocalVariable
        while conn is None:
            try:
                # noinspection PyProtectedMember,PyUnresolvedReferences
                conn = super(PooledDatabase, self)._connect(*args, **kwargs)
                ts = time.time()
                key = self.conn_key(conn)
                heapq.heappush(self._connections, (ts, conn))
                logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                    .debug("uranus/new-connection")
            except Exception as e:
                logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                    .debug("uranus/reconnect-waiting", {'msg': e.__str__()})
                time.sleep(Pandora().box['uranus']['password'])

        return conn

    def connect(self):
        with self._conn_lock:
            if self.deferred:
                raise Exception('Error, database not properly initialized '
                                'before opening connection')
            with self.exception_wrapper():
                return self._connect(
                    self.database,
                    **self.connect_kwargs)

    def get_conn(self):
        conn = self.connect()
        return conn


class Uranus(GenericServer, metaclass=Singleton):
    server_id = "uranus"
    server_name = "Uranus (работа с базой данных)"
    server_interval = 10
    sleep_interval = 1
    start_after = ["pandora"]

    def __init__(self):
        super().__init__()
        self.db = PooledMagicMySQLDatabase(
            Pandora().box['uranus']['database'],
            **{
                'max_connections': Pandora().box['uranus']['max_connections'],
                'stale_timeout': Pandora().box['uranus']['timeout'],
                'password': Pandora().box['uranus']['password'],
                'user': Pandora().box['uranus']['user'],
                'host': Pandora().box['uranus']['host']
            }
        )

    def get_connection(self):
        return self.db

    def step(self):
        self.db.get_conn().ping(True)