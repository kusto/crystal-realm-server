__author__ = 'crystal'

"""
Зевес тогді кружав сивуху
І оселедцем заїдав;
Він, сьому випивши восьмуху,
Послідки з кварти виливав.
Прийшла Венера, іскривившись.
Заплакалась и завіскрившись,
І стала хлипать перед ним:
"Чим пред тобою, милий тату,
Син заслужив таку мій плату?
Ійон, мов в свинки грають їм.

<...>

Юпітер, все допивши з кубка,
Погладив свій рукою чуб:
"Ох, доцю, ти моя голубка!
Я в правді твердий так, як дуб.
Еней збудує сильне царство
І заведе своє там панство:
Не малий буде він панок.
На панщину ввесь світ погонить,
Багацько хлопців там наплодить
І всім їм буде ватажок."
"""
# noinspection PyUnresolvedReferences
from core.athena.athena import Athena
# noinspection PyUnresolvedReferences
from core.hecate.hecate import Hecate
# noinspection PyUnresolvedReferences
from core.hermes.hermes import Hermes
# noinspection PyUnresolvedReferences
from core.cronus.cronus import Cronus
# noinspection PyUnresolvedReferences
from core.charon.charon import Charon
# noinspection PyUnresolvedReferences
from core.ares.ares import Ares
# noinspection PyUnresolvedReferences
from core.ariadne.ariadne import Ariadne
from core.pandora.pandora import Pandora
from core.cerberus.cerberus import Cerberus

# noinspection PyUnresolvedReferences
from physics.persephone import Persephone
from http.server import BaseHTTPRequestHandler, HTTPServer
import threading
from urllib.parse import urlparse, parse_qs
import json
# import code
# import readline
import logging
import psutil
import resource

from core.demeter.generic_server import GenericWorker
from .chess import Chess
from .token import Token


# noinspection PyPep8Naming
def ID(string):
    # noinspection PyBroadException
    try:
        return Cronus().objects[string]
    except:
        return None


# noinspection PyBroadException
class ZeusHTTP(BaseHTTPRequestHandler):
    # noinspection PyMethodMayBeStatic
    def h_shutdown(self):
        from core.demeter.loader import Loader

        Loader().stop()
        return None

    def h_eval(self):
        z = "Невозможно установить возвращаемое значение"
        try:
            try:
                z = eval(self.qc['code'][0], globals(), locals())
            except:
                try:
                    exec(self.qc['code'][0], globals(), locals())
                except:
                    pass
            return str(z)
        except:
            logging.exception("Eval не удался")

    # noinspection PyMethodMayBeStatic
    def h_time(self):
        return {"h": Cronus().current_hour}

    # noinspection PyMethodMayBeStatic
    def h_list(self):
        return Cronus().get_all(0)

    def h_kill(self):
        if 'id' in self.qc:
                if self.qc['id'][0] in Cronus().objects:
                    Cronus().objects[self.qc['id'][0]].hp = -10

        return {}

    # noinspection PyMethodMayBeStatic
    def h_threads(self):
        return {
            'cronus': int(Cronus().is_alive()),
            'charon': int(Charon().is_alive()),
            'pandora': int(Pandora().is_alive()),
            'cerberus': int(Cerberus().is_alive()),
            'ACL': int(Cerberus().acl.is_alive())
        }

    def h_cpu(self):
        ret = {
            "cpu_cores": psutil.cpu_percent(percpu=True),
            "mem": psutil.virtual_memory().percent
        }
        return ret


    def h_api(self):
        try:
            move = self.qc['move'][0]
        except:
            move = ""

        return Chess().handle(move)

    def h_login(self):
        if 'login' in self.qc and 'password' in self.qc:
            for i in Pandora().box['zeus']['users']:
                if i['login'] == self.qc['login'][0] and i['password'] == self.qc['password'][0]:
                    return {"token": Token().generate()}

        return {
            "token": ""
        }
    
    def h_validate(self):
        return {
            "ok": int(Token().validate(self.qc['token'][0]))
            }

    def do(self):
        ret = None

        filemap = {
            "login.html": "./app/html/login.html",

            "jquery.js": "./app/js/jquery.js",
            "login.js": "./app/js/login.js",
            "token.js": "./app/js/token.js",
            "engine.js": "./app/js/engine.js",
            "chess.js": "./app/js/chess.js",
            "main.js": "./app/js/main.js",
            "ui.js": "./app/js/ui.js",

            "login.css": "./app/css/login.css",
            "chess.css": "./app/css/chess.css",
            "bars.css": "./app/css/bars.css",
            "main.css": "./app/css/main.css",
            "ui.css": "./app/css/ui.css",
        }

        vfilemap = {
            "credits.html": "./app/html/credits.html",
            "chess.html": "./app/html/chess.html",
            "index.html": "./app/html/zeus.html",
        }

        if 'op' not in self.qc:
            self.qc['op'] = ['index.html']

        if self.qc['op'][0] in filemap:
            f = open(filemap[self.qc['op'][0]])
            s = f.read(10000000)
            f.close()
            return s

        if self.qc['op'][0] == 'do_login':
            ret = self.h_login()

        if self.qc['op'][0] == 'validate_token':
            ret = self.h_validate()

        if 'token' in self.qc and Token().validate(self.qc['token'][0]):

            if self.qc['op'][0] in vfilemap:
                f = open(vfilemap[self.qc['op'][0]])
                s = f.read(10000000)
                f.close()
                return s

            if self.qc['op'][0] == 'chess_api':
                return self.h_api()

            if self.qc['op'][0] == 'list':
                ret = self.h_list()
                
            if self.qc['op'][0] == 'time':
                ret = self.h_time()
                    
            if self.qc['op'][0] == 'shutdown':
                self.h_shutdown()
                        
            if self.qc['op'][0] == 'cpu':
                ret = self.h_cpu()
                            
            if self.qc['op'][0] == 'kill':
                ret = self.h_kill()
                                
            if self.qc['op'][0] == 'eval':
                ret = self.h_eval()
                
            if self.qc['op'][0] == 'threads':
                ret = self.h_threads()
                
            if self.qc['op'][0] == 'do_login':
                ret = self.h_login()

            if self.qc['op'][0] == 'validate_token':
                ret = self.h_validate()

        if ret:
            return json.dumps(ret)
        else:
            f = open("./app/html/login.html")
            s = f.read(100000)
            f.close()
            return s

    def do_GET(self):
        # noinspection PyAttributeOutsideInit
        self.qc = parse_qs(urlparse(self.path).query)
        self.send_response(200)

        try:
            if self.qc['op'][0] == 'set.gif':
                f = open("./app/image/set.gif", 'rb')
                s = f.read(100000)
                f.close()
                self.wfile.write(s)
                return

            if self.qc['op'][0] == 'board.gif':
                f = open("./app/image/board.gif", 'rb')
                s = f.read(100000)
                f.close()
                self.wfile.write(s)
                return

            if self.qc['op'][0] == 'nya':
                f = open("./app/image/nya.jpeg", 'rb')
                s = f.read(100000000)
                f.close()
                self.wfile.write(s)
                return

            if self.qc['op'][0] == 'font.woff2':
                f = open("./app/css/font.woff2", 'rb')
                s = f.read(100000000)
                f.close()
                self.wfile.write(s)
                return
        except:
            pass

        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(self.do().encode("utf-8"))

    def log_message(self, frmt, *args):
        pass


# noinspection PyBroadException
class Zeus(GenericWorker):
    server_id = "zeus"
    server_name = "Zeus (административный веб-интерфейс)"
    start_after = ["cronus", "charon", "pandora", "cerberus", "cerberus_acl"]

    def __init__(self):
        super().__init__()
        self.cronus = Cronus()
        self.server = None

    def serve(self):
        self.server = HTTPServer(("0.0.0.0", 5100), ZeusHTTP)
        try:
            self.server.serve_forever()
        except:
            pass

    def start(self):
        thr = threading.Thread(target=self.serve)
        thr.start()
        ars = globals()
        ars.update(locals())

    def stop(self):
        self.server.server_close()

    def process(self, data):
        if 'operation' not in data:
            return None

        if data['operation'] == "list":
            return self.cronus.get_all(data)

        if data['operation'] == "kill":
            return None
