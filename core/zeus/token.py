from core.demeter.singleton import Singleton
from core.demeter.token import *


class Token(metaclass=Singleton):
    def __init__(self):
        self.inst = TokenManager(tokenclass=TimeoutToken)

    def generate(self):
        return self.inst.create_token(timeout=3600)

    def validate(self, token):
        return self.inst.validate_token(token)
