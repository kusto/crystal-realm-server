__author__ = 'crystal'

import json

import chess
import chess.uci

from core.demeter.singleton import Singleton


# noinspection PyBroadException,PyBroadException
class Chess(metaclass=Singleton):
    def __init__(self):
        self.uci = chess.uci.engine = chess.uci.popen_engine("/usr/games/stockfish")
        self.uci.uci()

        self.board = chess.Board()

    def newgame(self):
        self.board.reset()

    def move(self, move):
        if move == "NEWGAME":
            self.newgame()
            return True

        try:
            self.board.push_san(move)
        except:
            try:
                mv = chess.Move.from_uci(move)
                if mv in self.board.legal_moves:
                    self.board.push(mv)
                else:
                    return False
            except:
                return False

        self.uci.position(self.board)
        bm = self.uci.go(movetime=1000)
        self.board.push(bm.bestmove)

        return True

    def handle(self, move):
        ok = 1 if self.move(move) else 0
        if self.board.is_game_over():
            if self.board.is_checkmate():
                result = "0 - 1" if self.board.turn == 0 else "1 - 0"
            else:
                result = "0,5 - 0,5"

            res = {
                "fen": self.board.fen(),
                "ok": ok,
                "over": 1,
                "result": result
            }

        else:
            res = {
                "fen": self.board.fen(),
                "ok": ok,
                "over": 0
            }

        return json.dumps(res)