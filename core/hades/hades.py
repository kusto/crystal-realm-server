__author__ = 'crystal'

"""
Здесь люди входят в скорбный град к мученьям
Здесь люди входят к муке вековой,
Здесь люди входят к падшим поколеньям.
Подвинут правдой вечный Зодчий мой:
Господня сила, разум всемогущий
Любви первоначальной дух святой
Меня создали прежде твари сущей,
Но после вечных, и мне века нет.
Оставь надежду всяк, сюда идущий!
"""

import logging
from core.ariadne.ariadne import Ariadne
from core.hermes.hermes import Hermes


# noinspection PyBroadException
class Hades:
    def __init__(self):
        self.messages = []

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def load(self):
        Ariadne().bind("hades_message", self.bind)

    def bind(self, data):
        try:
            message = data['msg']
            self.messages.append(message)
            Hermes().notify({
                'event': "hades_new_message",
                'data': {
                    'name': data['session']['name'],
                    'msg': data['msg']
                }
            })
        except:
            self.logger.exception("general/error")

        return None