"""
Лежа в пещере своей, в три глотки лаял огромный
Цербер, и лай громовой оглашал молчаливое царство.
Видя, как шеи у пса ощетинились змеями грозно,
Сладкую тотчас ему лепешку с травою снотворной
Бросила жрица, и он, разинув голодные пасти,
Дар поймал на лету. На загривках змеи поникли,
Всю пещеру заняв, разлегся Цербер огромный.
Сторож уснул, и Эней поспешил по дороге свободной
Прочь от реки, по которой никто назад не вернулся.
"""

import logging
import time

import core.cerberus.auth
import core.demeter.singleton
from core.ariadne.ariadne import Ariadne
from . import acl
from core.demeter.generic_server import GenericServer


class Cerberus(GenericServer, metaclass=core.demeter.singleton.Singleton):
    server_id = "cerberus"
    server_name = "Cerberus (управление сессиями)"
    server_interval = 2
    sleep_interval = 1
    start_after = ["pandora", "charon"]

    def __init__(self):
        super().__init__()

        self.sessions = None
        self.acl = acl.ACL()

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def load(self):
        self.sessions = core.cerberus.auth.AuthManager()
        Ariadne().bind("auth", self.bind_1_0)
        Ariadne().bind("cerberus", self.bind_1_1)
        self.acl.load()

    def step(self):
        dl = []
        for ses in self.sessions.sessions:
            if ses in self.sessions.sessions and time.time() - self.sessions.sessions[ses].last > 300:
                self.logger.debug(
                    "cerberus/cerberus/session-killed",
                    {'sid': self.sessions.sessions[ses]['cerberus_id']}
                )
                dl.append(ses)

        for item in dl:
            del (self.sessions.sessions[item])

    def bind_1_0(self, data):
        self.logger.debug("cerberus/cerberus/cerberus-request-1-0")
        dt = self.sessions.auth(data)
        return dt

    def bind_1_1(self, data):
        self.logger.debug("cerberus/cerberus/cerberus-request-1-1")

        resp = None

        if 'cerberus_operation' in data:

            if 'auth' == data['cerberus_operation']:
                resp = self.sessions.auth(data)

            if 'quit' == data['cerberus_operation']:
                resp = self.sessions.deauth(data)

        return resp

    def can(self, data, operation):
        # Because we CAT
        session = self.sessions.get(data)
        if not session or 'role' not in session:
            role = "guest"
        else:
            role = session['role']
            session.last = time.time()

        return self.acl.can(role, operation)