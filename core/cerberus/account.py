import peewee
from core.uranus.uranus import Uranus


class Account(peewee.Model):
    id = peewee.PrimaryKeyField()
    email = peewee.CharField()
    login = peewee.CharField()
    password = peewee.CharField()
    role = peewee.CharField()

    class Meta:
        db_table = 'account'
        database = Uranus().get_connection()

