import logging
import random
import time

from core.cerberus.account import Account
from .session import Session


# noinspection PyBroadException,PyBroadException,PyPep8
class AuthManager:
    def __init__(self):
        self.sessions = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def auth(self, data):
        try:
            for model in Account.select().where(
                            (Account.login == data['login']) &
                            (Account.password == data['password'])
            ):
                self.logger.debug("cerberus/auth/welcome", {'login': model.login})

                answer = {
                    'cerberus_token': random.randrange(1, 10000),
                    'cerberus_id': int(time.time())
                }

                ses = Session({
                    'cerberus_token': answer['cerberus_token'],
                    'cerberus_id': answer['cerberus_id'],
                    'role': model.role,
                    'login': model.login,
                    'id': model.id
                })

                self.sessions[answer['cerberus_id']] = ses
                return answer

            return {'msg': 'Login failed'}
        except:
            self.logger.exception("cerberus/auth/auth-error")
        return {'msg': 'Login fail'}

    def isset(self, data):
        try:
            self.sessions[data['cerberus_id']]
        except Exception:
            return False

        if self.sessions[data['cerberus_id']]['cerberus_token'] != data['cerberus_token']:
            self.logger.info("cerberus/auth/bad-token", {'sid': str(data['cerberus_id'])})
            return False

        return True

    def get(self, data):
        if self.isset(data):
            self.sessions[data['cerberus_id']].last = time.time()
            return self.sessions[data['cerberus_id']]
        else:
            return None

    def deauth(self, data):
        if self.isset(data):
            del self.sessions[data['cerberus_id']]




