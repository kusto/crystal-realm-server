__author__ = 'crystal'

# import re
import os
import json
import logging

from core.demeter.generic_server import GenericServer


# noinspection PyBroadException,PyBroadException,PyBroadException,PyBroadException
class ACL(GenericServer):
    server_id = "cerberus_acl"
    server_name = "RBACL (система управления доступом)"
    server_interval = 60
    sleep_interval = 1
    start_after = ["cerberus"]

    def __init__(self):
        super().__init__()
        self.rules = {}
        self._CONFIG_PATH = "./app/acl/rules"
        self._REL_PATH = "./app/acl/relations"

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def load(self):
        data = {}
        rels = {}
        lst = os.listdir(self._CONFIG_PATH)
        for name in lst:
            if os.path.isfile(os.path.join(self._CONFIG_PATH, name)):
                data = self.rule_load_file(os.path.join(self._CONFIG_PATH, name), data)

        lst = os.listdir(self._REL_PATH)
        for name in lst:
            if os.path.isfile(os.path.join(self._REL_PATH, name)):
                rels = self.ruledef_load_file(os.path.join(self._REL_PATH, name), rels)

        self.build_cache(rels, data)

        self.rules = data

    def step(self):
        self.load()

    def rule_load_file(self, filename, data):
        # self.logger.debug("cerberus/acl/file-loading", {'name': filename})
        try:
            lines = open(filename).readlines()
            for line in lines:
                try:
                    rule = json.loads(line)
                    if rule['role'] not in data:
                        data[rule['role']] = {}

                    if 'priority' not in rule:
                        rule['priority'] = 5

                    if 'allow' not in rule:
                        rule['allow'] = 1

                    if rule['operation'] not in data[rule['role']] or \
                            rule['priority'] > data[rule['role']][rule['operation']]['priority']:
                        data[rule['role']][rule['operation']] = {'allow': rule['allow'], 'priority': rule['priority']}
                except:
                    self.logger.info("cerberus/acl/broken-line", {'line': line})
        except:
            self.logger.warning("cerberus/acl/broken-file", {'name': filename})

        return data

    def ruledef_load_file(self, filename, data):
        # noinspection PyUnusedLocal
        try:
            lines = open(filename).readlines()
            for line in lines:
                # noinspection PyUnusedLocal
                try:
                    rule = json.loads(line)
                    if rule['sibling'] not in data:
                        data[rule['sibling']] = []

                    if 'enabled' not in rule:
                        rule['enabled'] = 1

                    if rule['enabled']:
                        data[rule['sibling']].append(rule['parent'])
                except Exception as e:
                    self.logger.info("cerberus/acl/broken-line", {'line': line})
        except Exception as e:
            self.logger.warning("cerberus/acl/broken-file", {'name': filename})

        return data

    def build_cache(self, relations, rules):
        _cache = {}
        _processing = {}

        def build_cache_item(item, rels, rls):
            if item not in rls:
                rls[item] = {}

            if item in _processing:
                self.logger.error("cerberus/acl/cycle-parent")
                return rls

            _processing[item] = True

            if item not in _cache:

                if item in rels:

                    # noinspection PyShadowingNames
                    for rel in rels[item]:
                        rls = build_cache_item(rel, rels, rls)
                        for operation in rls[rel]:
                            if rls[rel][operation]['allow']:
                                rls[item][operation] = {'allow': 1}
                            # endif
                        # endfor
                    # endfor
                # endif
            # endif
            del _processing[item]
            _cache[item] = True
            return rls

        for rel in relations:
            rules = build_cache_item(rel, relations, rules)

        return rules

    def can(self, role, operation):
        if role not in self.rules:
            self.logger.debug("cerberus/acl/rejected", {'req': operation, 'role': role})
            return False

        if operation not in self.rules[role]:
            self.logger.debug("cerberus/acl/rejected", {'req': operation, 'role': role})
            return False

        if not self.rules[role][operation]['allow']:
            self.logger.debug("cerberus/acl/rejected", {'req': operation, 'role': role})
            return False

        return True