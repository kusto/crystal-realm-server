__author__ = 'crystal'

import time


class Session:
    def __init__(self, data):
        self.data = data
        self.last = time.time()

    def __getitem__(self, item):
        if item in self.data:
            return self.data[item]

    def __setitem__(self, key, value):
        self.data[key] = value

    def __delitem__(self, key):
        if key in self.data:
            del(self.data['key'])

    def __contains__(self, item):
        return item in self.data

    def __str__(self):
        return str(self.data)