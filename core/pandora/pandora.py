"""
Она идет по гулкой темноте
Коварными неслышными шагами…
Дрожит  - свечи  колеблемое пламя,
Но крепко спит седой Эпиметей.

В тот дальний, вечно запертый покой
Пандору гонит злое любопытство…
Рассвет встаёт, и надо торопиться,
И – снят замок беспечною рукой.

Скрипят, как стонут, петли сундука,
А из него – клубком змеиным лезут
Хранимые в узилище железном –
Печаль, обман, обида и тоска…

И по земле – десятки тысяч бед
Спешат излиться болью и позором…
Роняет крышку глупая Пандора,
Надежду лишь не выпустив на свет.
"""

import json
import logging
import os

import core.demeter.singleton
from core.demeter.generic_server import GenericServer


# noinspection PyBroadException
class Pandora(GenericServer, metaclass=core.demeter.singleton.Singleton):
    server_id = "pandora"
    server_name = "Pandora (управление конфигурацией)"
    server_interval = 10
    sleep_interval = 1

    def __init__(self):
        super().__init__()
        self._config = {}
        self.box = None
        self._CONFIG_PATH = "./app/config/"

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.load()

    def load(self):
        self._config = {}

        for d, dirs, files in os.walk(self._CONFIG_PATH):
            for f in files:
                path = os.path.join(d, f)
                data = self._load_file(os.path.join(path))
                self._config = self._merge(self._config, data)

        self.box = self._config

    def step(self):
        self.load()

    def _load_file(self, filename):
        # self.logger.debug("Загружается файл настроек "+filename)
        data = {}
        try:
            data_file = open(filename)
            data = json.load(data_file)
        except:
            self.logger.warning("pandora/broken-file", {'name': filename})

        return data

    def _merge(self, a, b, path=None):
        if path is None:
            path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    self._merge(a[key], b[key], path + [str(key)])
                elif a[key] == b[key]:
                    pass
                else:
                    self.logger.error("pandora/files-conflict")
            else:
                a[key] = b[key]
        return a
