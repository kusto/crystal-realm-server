__author__ = 'crystal'

"""
ToDo: найти цитату
"""
import logging

from .quest import QuestDB, Quest
from .state import StateDB, State
from .passage import PassDB, Pass
from .entry import EntryDB, Entry
from core.demeter.singleton import Singleton
from core.hermes.hermes import Hermes
from core.ariadne.ariadne import Ariadne


# noinspection PyBroadException,PyPep8
class Hecate(metaclass=Singleton):
    def __init__(self):
        self.quests = {}

        self.entries = {}
        self.user_entries = {}

        self.lookup = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def load(self):
        """
        Добро пожаловать в код. Здесь заканчивается ваше будущее.
        Сегодня мы вновь поговорим о кешировании.
        Но если в прошлый раз мы кешировали лес, то сегодня нас ждет кеширование чегот-о, похожего на орграф без циклов
        Впрочем, хранить мы его все равно будем как лес.
        За связи между узлами отвечают флаги, с ними можно ознакомиться в Афине.
        Надеюсь, этот код никогда не попадет в продакшн.
        ЛЮД3N С0ПР0В0ЖДАВШNХ С0ЙДYT C YMA
        БЕ3YMHbIM
        """
        self.build_cache_quests()
        Ariadne().bind("hecate_get_quests", self.get_active_quests_user)
        Ariadne().bind("hecate_get_state", self.get_quests_user)

    """
    А знаете ли вы ?

    А знаете ли вы, что, поскольку в базе внешних ключей едва ли не больше, чем полей, тут не будет перепроверок ?
    А знаете ли вы, что почти на всех внешних ключах стоит опция on update cascade on delete cascade ?
    А знаете ли вы, что Peewee поддерживает внешние ключи, но делает это весьма неудобно ?
    """

    def build_cache_quests(self):
        self.build_cache_quest_roots()
        self.build_cache_quest_states()
        self.build_cache_quest_passages()

    def build_cache_quest_roots(self):
        quests = QuestDB.select()
        for single in quests:
            obj = Quest(single)
            self.quests[obj.id] = obj
            self.lookup[obj.gameid] = obj.id

    def build_cache_quest_states(self):
        states = StateDB.select()
        for single in states:
            obj = State(single)
            self.quests[obj.quest].states[obj.state] = obj

    def build_cache_quest_passages(self):
        passes = PassDB.select()
        for single in passes:
            obj = Pass(single)
            self.quests[obj.quest].states[obj.parent].siblings[obj.sibling] = obj

    def build_cache_entries(self, cid):
        for key in self.quests:
            if key not in self.entries:
                self.entries[key] = {}

        self.user_entries[cid] = {}

        ent = EntryDB.select().where(EntryDB.cid == cid)
        for single in ent:
            self.entries[single.quest][cid] = Entry(single)
            self.user_entries[cid][single.quest] = self.entries[single.quest][cid]

    def clear_cache_entries(self, cid):
        self.logger.debug("hecate/cache-cleaning", {'cid': cid})
        try:
            for key in self.quests:
                if cid in self.entries[key]:
                    del (self.entries[key][cid])
                    del (self.user_entries[cid][key])

            del (self.user_entries[cid])
        except:
            self.logger.exception("hecate/cache-cleaning-error")

    def get_state(self, char, quest):
        if quest in self.lookup:
            quest = self.lookup[quest]

        if quest not in self.quests:
            self.logger.info("hecate/unknown-quest", {'quest': quest})
            return -1

        if char.id not in self.entries[quest]:
            return -1

        return self.entries[quest][char.id].state

    def set_state(self,
                  char,
                  quest,
                  state=0,
                  no_check_can=False,
                  no_check_complete=False,
                  no_check_sibling=False,
                  ff_after_assign=False,
                  force=False):

        if force:
            no_check_can = True
            no_check_complete = True
            no_check_sibling = True
            ff_after_assign = True

        if quest in self.lookup:
            quest = self.lookup[quest]

        if quest not in self.quests:
            self.logger.info("hecate/unknown-quest", {'quest': quest})

        if char.id not in self.entries[quest]:
            if no_check_sibling or state == self.quests[quest].init_state:
                if not ff_after_assign:
                    return self.assign_quest(char, quest)
                else:
                    self.assign_quest(char, quest)
            else:
                return False

        if not no_check_sibling:
            if state not in self.quests[quest].states[self.entries[quest][char.id].state].siblings:
                self.logger.debug("hecate/state-no-pass",
                                  {'from': self.entries[quest][char.id].state, 'to': state})
                return False

        if (
                    (
                                no_check_can or
                                self.quests[quest].states[self.entries[quest][char.id].state].siblings[state].can(char)
                    )
                and
                    (
                                no_check_complete or
                                self.quests[quest].states[self.entries[quest][char.id].state].siblings[state].complete(
                                        char,
                                        self.entries[quest][char.id].data
                                )
                    )
        ):
            if not no_check_sibling:
                self.quests[quest].states[self.entries[quest][char.id].state].siblings[state].call(
                    char,
                    self.entries[quest][char.id].data
                )
            self.entries[quest][char.id].state = state
            self.entries[quest][char.id].active = 0 if self.quests[quest].states[
                self.entries[quest][char.id].state].terminal else 1
            self.entries[quest][char.id].data = self.quests[quest].states[state].generate_data()
            self.entries[quest][char.id].save()

            Hermes().notify_single(
                {
                    'event': 'hecate_quest_update',
                    'quest': self.quests[quest].gameid,
                    'state': state
                }, char.client)
            return True

        return False

    def assign_quest(self, char, quest):
        if quest in self.lookup:
            quest = self.lookup[quest]

        if quest not in self.quests:
            self.logger.info("hecate/unknown-quest", {'quest': quest})

        cid = char.id

        self.entries[quest][cid] = Entry(EntryDB())
        self.entries[quest][cid].cid = cid
        self.entries[quest][cid].quest = quest
        """
        Предполагаем, что разработчик не шизофреник, и квестов,
        которые заканчиваются прямо в начальной стадии в игре нет
        """
        self.entries[quest][cid].active = 1
        self.entries[quest][cid].state = self.quests[quest].init_state
        self.entries[quest][cid].data = self.quests[quest].states[self.quests[quest].init_state].generate_data()
        self.user_entries[cid][quest] = self.entries[quest][cid]
        self.entries[quest][cid].save()

        Hermes().notify_single(
            {
                'event': 'hecate_quest_update',
                'quest': quest,
                'state': self.quests[quest].init_state
            },
            char.client
        )

        return True

    def refresh_quests(self, char):
        for key in self.user_entries[char.id]:
            quest = self.quests[self.user_entries[char.id][key].quest]
            state = self.user_entries[char.id][key].state
            ns = None

            for s in quest.states[state].siblings:
                if quest.states[state].siblings[s].can(char) and \
                        quest.states[state].siblings[s].complete(char, self.user_entries[char.id][key].data):
                    ns = s

            if ns is not None:
                self.set_state(char, quest.id, ns)

    def get_active_quests(self, cid, flag=False):
        if cid not in self.user_entries:
            return {}

        ls = {}

        for key in self.user_entries[cid]:
            if self.user_entries[cid][key].active or flag:
                qname = self.quests[key].gameid
                ls[qname] = {
                    'state': self.user_entries[cid][key].state,
                    'data': self.user_entries[cid][key].data,
                    'active': self.user_entries[cid][key].active,
                }

        return {'quests': ls}

    def get_active_quests_user(self, data):
        return self.get_active_quests(data['session']['character_id_integer'])

    def get_quests_user(self, data):
        return self.get_active_quests(data['session']['character_id_integer'], True)