__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel


class State:
    def __init__(self, model):
        self.id = model.id
        self.quest = model.quest
        self.state = model.state
        self.terminal = model.terminal

        self.siblings = {}

    def generate_data(self):
        data = {}
        for key in self.siblings:
            self.siblings[key].generate_data(data)

        return data


class StateDB(BaseModel):
    id = IntegerField()
    quest = IntegerField()
    state = IntegerField()
    terminal = IntegerField()

    class Meta:
        db_table = 'hecate_state'