__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel
from core.athena.athena import Athena
from core.ares.ares import Ares
import json


# noinspection PyBroadException,PyBroadException,PyBroadException
class Pass:
    def __init__(self, model):
        self.quest = model.quest
        self.parent = model.parent
        self.sibling = model.sibling

        try:
            self.request = json.loads(model.request)
        except:
            self.request = {}

        try:
            self.requirement = json.loads(model.requirement)
        except:
            self.requirement = {}

        try:
            self.action = json.loads(model.action)
        except:
            self.action = {}

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def can(self, character):
        return True

    def complete(self, character, data):
        if 'kill' in self.request:

            if 'npc' in self.request['kill']:
                for cls in self.request['kill']['npc']:
                    if data['kill']['npc'][cls] < self.request['kill']['npc'][cls]:
                        return False

            if 'player' in self.request['kill']:
                if data['kill']['player'] < self.request['kill']['player']:
                    return False

        if 'talk' in self.request:
            return True if character.nhk_talk is not None and character.nhk_talk.cls == self.request['talk'] else False

        return True

    # noinspection PyUnusedLocal
    def call(self, character, data):
        if 'flag' in self.action:
            for flag in self.action['flag']:
                Athena().set(character, flag, self.action['flag'][flag])

        if 'skill' in self.action:
            for skill in self.action['skill']:
                Ares().add_skill(character, skill)

    def generate_data(self, data):
        if 'kill' in self.request:
            if 'kill' not in data:
                data['kill'] = {}

            if 'npc' in self.request['kill']:
                if 'npc' not in data['kill']:
                    data['kill']['npc'] = {}

                for cls in self.request['kill']['npc']:
                    data['kill']['npc'][cls] = 0

            if 'player' in self.request['kill']:
                data['kill']['player'] = 0


class PassDB(BaseModel):
    quest = IntegerField()
    parent = IntegerField()
    sibling = IntegerField()
    request = TextField()
    action = TextField()
    requirement = TextField()

    class Meta:
        db_table = 'hecate_pass'


