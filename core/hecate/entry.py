__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel
import json


# noinspection PyBroadException
class Entry:
    def __init__(self, model):
        self.db = model
        self.cid = model.cid
        self.quest = model.quest
        self.state = model.state

        self.active = model.active

        self.wrapper = None

        try:
            self.data = json.loads(model.data)
        except:
            self.data = {}

    def save(self):
        self.db.cid = self.cid
        self.db.quest = self.quest
        self.db.state = self.state
        self.db.active = self.active
        self.db.data = json.dumps(self.data)
        return self.db.save()

    def on_kill(self, killed):
        if 'kill' in self.data:
            if killed.type == 1 and 'player' in self.data['kill']:
                self.data['kill']['player'] += 1
                self.save()

            if killed.type == 3 and 'npc' in self.data['kill'] and killed.classstr in self.data['kill']['npc']:
                self.data['kill']['npc'][killed.classstr] += 1
                self.save()


class EntryDB(BaseModel):
    cid = IntegerField()
    quest = IntegerField()
    state = IntegerField()
    data = TextField()
    active = IntegerField()

    class Meta:
        db_table = 'hecate_entry'