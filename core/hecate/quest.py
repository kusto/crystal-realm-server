__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel
import json


# noinspection PyBroadException
class Quest:
    def __init__(self, model):
        self.id = model.id
        self.gameid = model.gameid
        self.init_state = model.init_state

        try:
            self.requirement = json.loads(model.requirement)
        except:
            self.requirement = None

        self.states = {}

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def can(self, character):
        return True


class QuestDB(BaseModel):
    gameid = CharField(unique=True)
    id = IntegerField(primary_key=True)
    init_state = IntegerField()
    requirement = TextField()

    class Meta:
        db_table = 'hecate_quest'
