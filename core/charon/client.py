import time
import logging
from core.ariadne.ariadne import Ariadne


class Client(object):

    def __init__(self, charon, sock):
        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        now = time.time()

        self.charon = charon

        self.addr = sock.getpeername()
        self.sock = sock
        self.logger.debug("charon/client/connected",
                          {
                              'addr': self.addr[0],
                              'port': self.addr[1]
                          })

        self.lastcmd = now
        self.init = now

        self.input = ""
        self.output = ""

        charon.clients[sock] = self
        charon.connections.update((sock,))

    def step(self):
        now = time.time()
        data = self.input.split("\n")
        # logging.debug(data)
        self.input = data.pop()

        if data:
            for command in data:
                temp = Ariadne().run(command, client=self)
                if temp:
                    self.output += temp
                self.lastcmd = time.time()
                # logging.debug(command+" "+self.output)

        if now - self.lastcmd > 30:
            self.logger.debug("charon/client/kicked",
                              {
                                  'addr': self.addr[0],
                                  'port': self.addr[1]
                              })
            self.close()

    def append(self, stirng):
        self.output += stirng

    def send(self, data):
        # logging.debug(data)
        return self.sock.send(data.encode('utf-8'))

    def close(self):
        self.charon.connections.difference_update((self.sock,))
        self.charon.request_close(self.sock)
        self.input = ""
        self.output = ""