"""
Мрачный и грязный Харон. Клочковатой седой бородою
Все лицо обросло — лишь глаза горят неподвижно,
Плащ на плечах завязан узлом и висит безобразно.
Гонит он лодку шестом и правит сам парусами,
Мертвых на утлом челне через темный поток перевозит.
Бог уже стар, но хранит он и в старости бодрую силу.
"""

import logging
import socket
import select

import core.charon.client
import core.demeter.singleton
from core.ariadne.ariadne import Ariadne
from core.pandora.pandora import Pandora
from core.demeter.generic_server import GenericServer


# noinspection PyBroadException
class Charon(GenericServer, metaclass=core.demeter.singleton.Singleton):
    server_id = "charon"
    server_name = "Charon (сетевой менеджер)"
    server_interval = 0.05
    sleep_interval = 0.02
    start_after = ["pandora"]

    """
    def broadcast_data(self, message, sock=None):
        # Рассылка сообщения по всем клиентам
        for insocket in self.connections:
            if insocket != self.server_socket and insocket != sock:
                try:
                    insocket.send(message.encode())
                except Exception as e:
                    # broken socket connection may be, chat client pressed ctrl+c for example
                    logging.warning(e.__str__())
                    logging.info("Отключен нерабочий клиент")
                    insocket.close()
                    self.connections.remove(insocket)

    def send_data(self, message, insocket):
        try:
            insocket.send(message.encode())
        except Exception as e:
            # broken socket connection may be, chat client pressed ctrl+c for example
            logging.warning(e.__str__())
            logging.info("Отключен нерабочий клиент")
            insocket.close()
            self.connections.remove(insocket)
    """

    def __init__(self):
        super().__init__()

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.connections = set([])
        self.clients = {}
        self.buffer_size = 4096
        try:
            self.port = Pandora().box['charon']['port']
            self.delay = Pandora().box['charon']['interval']
            self._pong = Pandora().box['charon']['pong']
        except:
            self.logger.warning("charon/charon/config-error")
            self.port = 5000
            self.delay = 1.0
            self._pong = 0

        self.server_socket = None

        self.binds = {}

        self._close_list = []

        try:
            self._surprise_motherfucker = Pandora().box['charon']['surprise_motherfucker']
        except:
            self._surprise_motherfucker = 0

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def pong(self, data):
            return {'answer': 'pong'}

    def load(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(("0.0.0.0", self.port))
        self.server_socket.listen(10)

        # set
        self.connections = {self.server_socket}

        # dict
        self.clients = {}

        if self._pong > 0:
            Ariadne().bind("ping", self.pong)

    # noinspection PyMethodMayBeStatic
    def bind(self, bind_type, function):
        try:
            if Pandora().box['loader']['bind_backport'] == 1:
                Ariadne().bind(bind_type, function)
        except:
            pass

    @staticmethod
    def _client_has_data(client):
        return client.output

    @staticmethod
    def _client_get_socket(client):
        return client.sock

    def step(self):
        wr = map(Charon._client_get_socket, filter(Charon._client_has_data, self.clients.values()))
        readable, writeable, errors = select.select(self.connections, wr, self.connections, self.delay)
        for sock in writeable:
            try:
                if sock in self.clients:
                    cli = self.clients[sock]
                    if cli.output:
                        sent = cli.send(cli.output)
                        if sent == 0:
                            self.logger.debug("charon/charon/write-error")
                            errors.append(sock)
                        else:
                            cli.output = cli.output[sent:]
            except:
                self.logger.warning("charon/charon/write-error")
                errors.append(sock)

        for sock in readable:
            # Входящее соединение
            try:
                if sock in self.clients:
                    try:
                        data = sock.recv(self.buffer_size)
                        data = data.decode('utf-8')
                        self.clients[sock].input += data

                    except:
                        self.logger.info("charon/charon/io-error")
                        continue
                else:
                    try:
                        # Если это новое соединение
                        sockfd, addr = self.server_socket.accept()
                        core.charon.client.Client(self, sockfd)
                    except:
                        self.logger.info("charon/charon/io-error")
            except:
                self.logger.info("charon/charon/io-error")
                errors.append(sock)

        for sock in errors:
            try:
                cli = self.clients[sock]
                if cli:
                    cli.close()
                else:
                    self.logger.info("charon/charon/close-error")
            except:
                self.logger.exception("charon/charon/close-error")

        for key in self.clients:
            try:
                self.clients[key].step()
            except:
                self.logger.exception("general/error")

        self._close_delayed()

    def _close_delayed(self):
        self.clients = dict((key, value) for key, value in self.clients.items() if key not in self._close_list)
        for c in self._close_list:
            c.close()
        self._close_list = []

    def request_close(self, key):
        self._close_list.append(key)