__author__ = 'crystal'

"""
Порой казалось, достигает слух
Тех двух других, которые, должно быть,
Не отстают при этом восхожденье.
И снова только  звук его шагов,
И снова только ветер за спиною.
Они идут - он громко говорил,
Чтобы услышать вновь, как стихнет голос.
И все-таки идут они, те двое,
Хотя и медленно. Когда бы мог
Он обернуться (если б обернувшись,
Он своего деянья не разрушил,
Едва-едва свершенного) - увидеть
Он мог бы их, идущих тихо следом.

Вот он идет, бог странствий и вестей,
Торчит колпак над светлыми глазами,
Мелькает посох тонкий перед ним,
Бьют крылья по суставам быстрых ног,
Ее ведет он левою рукою.
"""

import logging

import core.ariadne.io
import core.demeter.singleton
from core.ariadne.ariadne import Ariadne
from core.charon.charon import Charon


# noinspection PyBroadException,PyBroadException
class Hermes(metaclass=core.demeter.singleton.Singleton):
    def __init__(self):
        self.pools = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def add_listener(self, pool, listener):
        # logging.debug("Listen: "+str(pool)+" "+str(listener))
        if pool not in self.pools:
            self.pools[pool] = set()

        self.pools[pool].add(listener)

    def remove_listener(self, pool, listener):
        # logging.debug("REM: "+str(pool)+" "+str(listener))
        try:
            self.pools[pool].remove(listener)
        except:
            self.logger.exception("hermes/listener-remove-error")

    def global_silence(self, listener):
        for pool in self.pools:
            if listener in self.pools[pool]:
                self.pools[pool].remove(listener)

    def notify(self, event, pool=None):
        if isinstance(event, dict):
            event['type'] = "event"
        if pool is None:
            return self.notify_fallback(event)
        # logging.debug(self.pools)

        if pool not in self.pools:
            self.pools[pool] = set()

        string = core.ariadne.io.Output.format(event)

        for i in self.pools[pool]:
            try:
                i.output += string
            except:
                self.global_silence(i)
                self.logger.exception("hermes/listener-remove-error")

    # noinspection PyMethodMayBeStatic
    def notify_fallback(self, event):
        string = core.ariadne.io.Output.format(event)
        # logging.debug(string)

        for i in Charon().clients:
            Charon().clients[i].output += string
            # logging.debug(i)

    # noinspection PyMethodMayBeStatic
    def notify_single(self, event, client):
        if isinstance(event, dict):
            event['type'] = "event"
        string = core.ariadne.io.Output.format(event)
        client.output += string