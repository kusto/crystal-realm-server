__author__ = 'crystal'

import re
import logging


# noinspection PyBroadException
class Logic:
    def __init__(self, athena):
        self.athena = athena
        self.hecate = None
        self.cronus = None

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def evaluate(self, string, character=None):
        ss = string

        def getflag(match):
            flag = match.group(0)
            if flag == "CRONUS_TIME" and self.cronus:
                return str(self.cronus.current_hour)

            z = self.athena.get(character, flag)

            if not z and self.hecate:
                z = self.hecate.get_state(character, flag)

            return str(z)

        string = re.sub('([A-Za-z_]+)', getflag, string)

        string = string.replace("~", "not ")
        string = string.replace("&", " and ")
        string = string.replace("|", " or ")
        try:
            return eval(string)
        except:
            self.logger.exception(
                "athena/logic-broken/expression",
                {'expr': ss, 'parsed': string}
            )
            return False
