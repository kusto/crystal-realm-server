__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel
from .flag import Flag


class Value(BaseModel):
    cid = IntegerField(db_column='cid')
    flag = ForeignKeyField(db_column='flag', rel_model=Flag, to_field='id')
    value = IntegerField()

    class Meta:
        db_table = 'athena_values'
