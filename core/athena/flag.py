__author__ = 'crystal'

from peewee import *
from .basemodel import BaseModel


class Flag(BaseModel):
    name = CharField(unique=True)

    class Meta:
        db_table = 'athena_flags'