__author__ = 'crystal'

"""
Пусть поспорит со мной! Проиграю - отдам что угодно".
Облик старухи приняв, виски посребрив сединою
Ложной, Паллада берет, - в поддержку слабого тела, -
Посох и говорит ей: "Не все преклонного возраста свойства
Следует нам отвергать: с годами является опыт.
Не отвергай мой совет. Ты в том домогаешься славы,
Что обрабатывать шерсть всех лучше умеешь из смертных.
Перед богиней склонись и за то, что сказала, прощенья,
Дерзкая, слезно моли. Простит она, если попросишь".
Искоса глянула та, оставляет начатые нити;
Руку едва удержав, раздраженье лицом выражая,
Речью Арахна такой ответила скрытой Палладе:
"Глупая ты и к тому ж одряхлела от старости долгой!
Жить слишком долго - во вред. Подобные речи невестка
Слушает пусть или дочь, - коль дочь у тебя иль невестка.
Мне же достанет ума своего. Не подумай, совета
Я твоего не приму, - при своем остаюсь убежденье.
Что ж не приходит сама? Избегает зачем состязанья?"
Ей же богиня, - "Пришла!" - говорит и, образ старухи
Сбросив, явила себя.
"""

from .flag import Flag
from .value import Value
from .logic import Logic
from core.hermes.hermes import Hermes
from core.demeter.singleton import Singleton
from core.ariadne.ariadne import Ariadne


class Athena(metaclass=Singleton):
    def __init__(self):
        self.flags = {}
        self.flags_db = {}
        self.flags_ent = {}

        self.logic = Logic(self)

    def load(self):
        """
        Очередной прогрев кеша.
        Я искренне надеюсь, что оно того стоит.
        """
        flags = Flag.select()
        for single in flags:
            self.flags[single.name] = {}
            self.flags_db[single.name] = {}
            self.flags_ent[single.name] = single.id

        values = Value.select()
        for single in values:
            self.flags[single.flag.name][single.cid] = single.value
            self.flags_db[single.flag.name][single.cid] = single

        Ariadne().bind("athena_flags", self.get_flags)

    def get(self, character, flag):
        if flag in self.flags and character.id in self.flags[flag]:
            return self.flags[flag][character.id]

        return 0

    def set(self, character, flag, value):
        if flag not in self.flags:
            return False

        if character.id not in self.flags[flag]:
            self.flags_db[flag][character.id] = Value()
            self.flags_db[flag][character.id].flag = self.flags_ent[flag]
            self.flags_db[flag][character.id].cid = character.id

        self.flags[flag][character.id] = value
        self.flags_db[flag][character.id].value = value
        self.flags_db[flag][character.id].save()

        Hermes().notify_single({"event": "athena_flag_update", "flag": flag, "value": value}, character.client)
        return True

    def get_flags(self, data):
        character = data['session']['character_id_integer']
        return {
            'type': "athena_flags",
            "spisok": {
                flag: self.flags[flag][character] for flag in self.flags if character in self.flags[flag]
            }
        }

    def evaluate(self, character, expr):
        return self.logic.evaluate(expr, character)
