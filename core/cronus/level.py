__author__ = 'crystal'

from core.hermes.hermes import Hermes
from core.demeter.i18n import I18n


class LevelManager:
    def __init__(self, cronus):
        self.cronus = cronus

    def xp(self, killer, killed):
        xp = killed.level * 100

        if killer.group:
            xp /= 2
            xp /= len(killer.group.members)
            xp = round(xp)

            for i in killer.group:
                if i in self.cronus.objects:
                    self.cronus.objects[i].add_xp(xp)
        else:
            killer.add_xp(xp)

    # noinspection PyMethodMayBeStatic
    def distribute_skillpoints(self, character, data):
        st = data['str'] if 'str' in data else 0
        agi = data['agi'] if 'agi' in data else 0
        inte = data['int'] if 'int' in data else 0
        luc = data['luc'] if 'luc' in data else 0

        if st + agi + inte + luc > character.skillpoints:
            # noinspection PyProtectedMember
            return I18n()._("cronus/level/injection-rejection")

        if st < 0 or agi < 0 or luc < 0 or inte < 0:
            # noinspection PyProtectedMember
            return I18n()._("cronus/level/injection-rejection")

        character.skillpoints -= st
        character.stats.str += st

        character.skillpoints -= agi
        character.stats.agi += agi

        character.skillpoints -= inte
        character.stats.int += inte

        character.skillpoints -= luc
        character.stats.luc += luc

        character.save()

        Hermes().notify_single({'event': "hermes_cronus_state_change", 'data': character.dump()}, character.client)

        return None