__author__ = 'crystal'

from peewee import *
from . import basemodel


class LocationDB(basemodel.BaseModel):
    freepvp = IntegerField()
    gameid = CharField()
    name = CharField()
    squares = TextField()
    respawn = TextField()

    class Meta:
        db_table = 'location'
