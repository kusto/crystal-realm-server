__author__ = 'crystal'

import json
from . import square
import logging
from core.demeter.i18n import I18n


# noinspection PyBroadException,PyBroadException
class Location:
    def __init__(self, manager):
        self.squares = []
        self.freepvp = 0
        self.gameid = "abyss"
        # noinspection PyProtectedMember
        self.name = I18n()._("cronus/location/location-default-name")
        self.manager = manager
        self.respawn = {'x': 0, 'y': 0, 'z': 0}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def fromdb(self, model):
        self.freepvp = model.freepvp
        self.name = model.name
        self.gameid = model.gameid
        try:
            dg = json.loads(model.squares)
            for i in dg:
                self.squares.append(square.Square(i[0], i[1], self))
                self.manager.bind_square((i[0], i[1]), self)
        except:
            self.logger.exception("cronus/location/squares-load-fail", {'location': self.gameid})

        try:
            dg = json.loads(model.respawn)
            self.respawn = dg
        except:
            self.logger.exception("cronus/location/respawn-load-fail", {'location': self.gameid})

        return self

    def get_respawn(self):
        return self.respawn