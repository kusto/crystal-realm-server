__author__ = 'crystal'


class Square:
    def __init__(self, x, y, location):
        self.x = x
        self.y = y
        self.location = location

    def get_near(self):
        return [{'x': x, 'y': y} for x in range(self.x-1, self.x+1) for y in range(self.y-1, self.y+1)]

    def get_respawn(self):
        return {
            'x': self.x * 100 + 50,
            'y': self.y * 100,
            'z': 0
        }