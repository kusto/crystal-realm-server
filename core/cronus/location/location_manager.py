__author__ = 'crystal'

from . import location_db, location
import logging


class LocationManager:
    def __init__(self):
        self.squares = {}
        self.locations = {}

        self.abyss = location.Location(self)

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        locs = location_db.LocationDB.select()

        for loc in locs:
            obj = location.Location(self).fromdb(loc)
            self.locations[obj.gameid] = obj
            self.logger.debug("cronus/location/location-loaded", {'location': obj.gameid})

    def bind_square(self, square, loc):
        self.squares[square] = loc

    # noinspection PyMethodMayBeStatic
    def get_square(self, x, y):
        return ((int((x+50)/100),
                int((y+50)/100)))

    # noinspection PyMethodMayBeStatic
    def get_area(self, x, y):
        return [(i, j) for i in range(x - 1, x + 2) for j in range(y - 1, y + 2)]

    def get_location(self, square):
        if square in self.squares:
            return self.squares[square]

        return self.abyss