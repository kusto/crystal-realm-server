__author__ = 'crystal'

from . import group
import time
import logging
from core.hermes.hermes import Hermes
from core.demeter.i18n import I18n


# noinspection PyBroadException,PyBroadException,PyBroadException,PyBroadException,PyBroadException
class GroupManager:
    def __init__(self, cronus):
        self.groups = set()
        self.invitations = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.cronus = cronus

    def invite(self, sender, receiver):
        if sender.gameid == receiver.gameid:
            return False
        if receiver.group is not None:
            return False

        tm = str(time.time())

        self.invitations[tm] = {
            'sender': sender,
            'receiver': receiver
        }

        Hermes().notify_single(
            {
                'event': 'cronus_group_invite',
                'sender': sender.name,
                'invite': tm
            }, receiver.client)

        return True

    def accept(self, invite):
        if invite not in self.invitations:
            return False

        sender = self.invitations[invite]['sender']
        receiver = self.invitations[invite]['receiver']

        if sender.group is None:
            sender.group = group.Group(sender, self.cronus)

        sender.group.add(receiver)

        return True

    def decline(self, invite):
        if invite not in self.invitations:
            return False

        del (self.invitations[invite])

        return True

    def cmd_invite(self, data):
        if data['receiver'] in self.cronus.objects and self.cronus.objects[data['receiver']].type == 1:
            receiver = self.cronus.objects[data['receiver']]
        else:
            # noinspection PyProtectedMember
            return I18n()._("cronus/group/msg/invite-fail")

        sender = self.cronus.objects[data['session']['character_id']]

        if self.invite(sender, receiver):
            return None
        else:
            # noinspection PyProtectedMember
            return I18n()._("cronus/group/msg/invite-send-fail")

    def cmd_accept(self, data):
        try:
            if self.accept(data['invite']):
                return None
        except:
            logging.exception("")

        # noinspection PyProtectedMember
        return I18n()._("cronus/group/msg/invite-accept-fail")

    def cmd_decline(self, data):
        try:
            if self.decline(data['invite']):
                return None
        except:
            logging.exception("")

        # noinspection PyProtectedMember
        return I18n()._("cronus/group/msg/invite-decline-fail")

    def cmd_leave(self, data):
        try:
            char = self.cronus.objects[data['session']['character_id']]
            if char.group:
                char.group.remove(char)
                char.group = None
                Hermes().notify_single({'event': 'cronus_group_leave'}, char.client)
            else:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/leave-no-group")
        except:
            # noinspection PyProtectedMember
            return I18n()._("cronus/group/msg/leave-fail")

    def cmd_disband(self, data):
        try:
            char = self.cronus.objects[data['session']['character_id']]
            if char.group is None:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/disband-no-group")

            if char.group.leader != char.gameid:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/disband-no-rights")

            return char.group.unform()
        except:
            # noinspection PyProtectedMember
            return I18n()._("cronus/group/msg/disband-fail")

    def cmd_set_pahan(self, data):
        try:
            char = self.cronus.objects[data['session']['character_id']]
            if char.group is None:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/pahan-no-group")

            if char.group.leader != char.gameid:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/pahan-no-rights")

            if data['pahan'] not in char.group.members:
                # noinspection PyProtectedMember
                return I18n()._("cronus/group/msg/pahan-new-fail")

            char.group.set_pahan(data['pahan'])

        except:
            # noinspection PyProtectedMember
            return I18n()._("cronus/group/msg/pahan-fail")