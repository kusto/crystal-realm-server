__author__ = 'crystal'

import logging
from core.demeter.i18n import I18n
from core.hermes.hermes import Hermes


# noinspection PyBroadException
class Group:
    def __init__(self, leader, cronus):
        self.members = {leader.gameid}
        self.leader = leader.gameid
        self.cronus = cronus

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        Hermes().add_listener(self, leader.client)

    def add(self, member):
        self.members.add(member.gameid)
        member.group = self

        members = {gid: self.cronus.objects[gid].dump() for gid in self.members}

        Hermes().add_listener(self, member.client)

        Hermes().notify(
            {
                'event': 'cronus_group_update',
                'members': members,
                'pahan': self.leader
            }, self)

    def remove(self, member):
        if member.gameid in self.members:
            self.members.remove(member.gameid)
            Hermes().remove_listener(self, member.client)

            if len(self.members) <= 1:
                return self.unform()

            if self.leader == member.gameid:
                for i in self.members:
                    self.leader = i
                    break

            members = {gid: self.cronus.objects[gid].dump() for gid in self.members}
            Hermes().notify(
                {
                    'event': 'cronus_group_update',
                    'members': members
                }, self)

    def unform(self):
        Hermes().notify(
            {
                'event': 'cronus_group_leave',
            }, self)

        for i in self.members:
            try:
                self.cronus.objects[i].group = None
                Hermes().remove_listener(self, self.cronus.objects[i].client)
            except:
                pass

        # noinspection PyProtectedMember
        return {'type': 'system_message', 'msg': I18n()._("cronus/group/msg/group-unformed")}

    def set_pahan(self, pahan):
        self.leader = pahan
        members = {gid: self.cronus.objects[gid].dump() for gid in self.members}
        Hermes().notify(
            {
                'event': 'cronus_group_update',
                'members': members,
                'pahan': self.leader
            }, self)