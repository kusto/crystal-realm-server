__author__ = 'crystal'

from .model import pc
import logging
from core.cerberus.cerberus import Cerberus


# noinspection PyBroadException,PyBroadException,PyBroadException
class CharacterManager:
    def __init__(self, cronus):
        self.cronus = cronus

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def bind_2(self, data):
        if 'op' not in data:
            return None

        if data['op'] == 'list_characters':
            return self.get_characters(data)

        if data['op'] == 'select_character':
            return self.select_character(data)

        if data['op'] == 'delete_character':
            return self.delete_character(data)

        return None

    def get_characters(self, data):
        def frmt(dt):
            return {
                'id': dt.id,
                'name': dt.name,
                'level': dt.level
            }

        try:
            ses = Cerberus().sessions.get(data)
            chars = pc.PlayerDB.select().where((pc.PlayerDB.account == ses['id'])
                                               & ((pc.PlayerDB.statuscode == 0) | (pc.PlayerDB.statuscode == 4)))
            return {"data": [frmt(model) for model in chars]}
        except:
            self.logger.exception("general/error")
            return None

    def select_character(self, data):
        def create(i, name):
            return pc.PlayerDB.create(
                account=i,
                name=name,
                level=1,
                xp=0,
                position='{"new":1}',
                stats='{"new":1}',
            )

        try:
            ses = Cerberus().sessions.get(data)
            if data['id'] == 0:
                char = create(ses['id'], data['name'])
                obj = pc.PlayerCharacter(cronus=self.cronus)
                obj.client = ses['client']
                obj.fromdb(char)
                self.cronus.add(obj)
                ses['character_id'] = obj.gameid
                """
                Жаль, что в питоне нет @undocumented, а писать его самому мне лень.
                Короче, это недокументированное поле.
                """
                ses['character_id_integer'] = obj.id
                return {"type": "cronus_ngenesis", "gameid": obj.gameid}

            chars = pc.PlayerDB.select().where((pc.PlayerDB.account == ses['id'])
                                               & (pc.PlayerDB.id == data['id'])
                                               & ((pc.PlayerDB.statuscode == 0)
                                                  | (pc.PlayerDB.statuscode == 4))) \
                .limit(1)

            for char in chars:
                obj = pc.PlayerCharacter(cronus=self.cronus)
                obj.fromdb(char)
                if obj.gameid in self.cronus.objects:
                    obj = self.cronus.objects[obj.gameid]
                    obj.client = ses['client']
                    obj.reload()
                else:
                    obj.client = ses['client']
                    self.cronus.add(obj)

                ses['character_id'] = obj.gameid
                """
                См. комментарий выше
                """
                ses['character_id_integer'] = obj.id
                return {"type": "cronus_ngenesis", "gameid": obj.gameid}
            return {"type": "cronus_ngenesis"}

        except:
            self.logger.exception("general/error")
            return {"type": "cronus_ngenesis"}

    def delete_character(self, data):
        # noinspection PyUnusedLocal
        try:
            ses = Cerberus().sessions.get(data)
            chars = pc.PlayerDB.select().where((pc.PlayerDB.account == ses['id'])
                                               & (pc.PlayerDB.id == data['id'])
                                               & (pc.PlayerDB.statuscode == 0))

            for char in chars:
                char.statuscode = 1
                char.save()
                return None
            return None

        except Exception as e:
            self.logger.exception("general/error")
            return None
