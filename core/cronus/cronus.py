__author__ = 'crystal'

"""
Все бормочут свои молитвы и смотрят на небо. Но боги никогда не ответят им.
"""
# noinspection PyUnresolvedReferences
import logging
import time

# noinspection PyUnresolvedReferences
from .model import static, static_class
from .model import npc, npc_class
from .model import enemy, enemy_class
from .model import nyanchan
# noinspection PyUnresolvedReferences
from .model import pc

from .location import location_manager
from .group import group_manager

from core.demeter.generic_server import GenericServer
from core.demeter.hook_manager import HookManager
from core.demeter.singleton import Singleton
from core.ariadne.ariadne import Ariadne
from core.pandora.pandora import Pandora
# noinspection PyUnresolvedReferences
from core.cerberus.cerberus import Cerberus

from . import characters, level
from .plugin import *


# noinspection PyBroadException,PyBroadException
class Cronus(GenericServer, metaclass=Singleton):
    server_id = "cronus"
    server_name = "Cronus (система игрового мира)"
    # server_interval - см. load
    # sleep_interval - см. load
    start_after = ["pandora", "persephone", "uranus", "cerberus_acl"]

    def __init__(self):
        super().__init__()

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.classes = {}
        self.objects = {}
        self.enemy_classes = {}
        self.static_classes = {}
        self.to_add = []
        self.to_delete = []

        self.last = time.time()

        self.lm = location_manager.LocationManager()
        self.gm = group_manager.GroupManager(self)
        self.cm = characters.CharacterManager(self)
        self.level = level.LevelManager(self)

        """
        Спасибо всем богам за то, что в питон встроена длинка
        """
        self.current_tick = 0
        self.current_tick_normalized = 300000
        self.current_hour = 0

        self.ticks_per_hour = 1
        self.ticks_per_second = 1

    def load(self):
        r00t = nyanchan.NyanChan(0, self)
        r00t.position.z = 5
        self.add(r00t)

        self.logger.info("cronus/cronus/loading-npc-classes")
        npc_classes = npc_class.NPCClassDB.select()
        for c in npc_classes:
            self.classes[c.id] = npc_class.NPCClass().fromdb(c)

        self.logger.info("cronus/cronus/loading-npc")

        npcs = npc.NPCharacterDB.select().where(npc.NPCharacterDB.enabled == 1)
        for c in npcs:
            self.add(npc.NPCharacter(cronus=self).fromdb(c))

        self.logger.info("cronus/cronus/loading-enemy-classes")
        enemy_classes = enemy_class.EnemyClassDB.select()
        for c in enemy_classes:
            self.enemy_classes[c.id] = enemy_class.EnemyClass().fromdb(c)

        self.logger.info("cronus/cronus/loading-enemy")

        enemytati = enemy.EnemyDB.select()
        for c in enemytati:
            self.add(enemy.EnemyCharacter(cronus=self).fromdb(c))

        Ariadne().bind("cronus_all", self.get_all)

        Ariadne().bind("cronus", self.cm.bind_2)

        Ariadne().bind("cronus_mendel", self.mendel)

        Ariadne().bind("cronus_pinus", self.pinus)

        Ariadne().bind("cronus_suc", self.distribute_skillpoints)

        Ariadne().bind("cronus_group_invite", self.gm.cmd_invite)
        Ariadne().bind("cronus_group_accept", self.gm.cmd_accept)
        Ariadne().bind("cronus_group_decline", self.gm.cmd_decline)
        Ariadne().bind("cronus_group_leave", self.gm.cmd_leave)
        Ariadne().bind("cronus_group_disband", self.gm.cmd_disband)
        Ariadne().bind("cronus_group_set_pahan", self.gm.cmd_set_pahan)

        self.ticks_per_second = Pandora().box['cronus']['ticks_per_second']
        self.server_interval = 1 / self.ticks_per_second
        self.sleep_interval = self.server_interval / 2

        self.ticks_per_hour = Pandora().box['cronus']['ticks_per_game_hour']

        autokick.Autokick()
        persephone.Persephone()
        hermes.Hermes()
        regeneration.Regeneration()

    def add(self, obj):
        self.to_add.append(obj)

    def remove(self, obj):
        self.to_delete.append(obj)

    def step(self):
        self.current_tick += 1
        self.current_tick_normalized += 1
        if self.current_tick_normalized > 24 * self.ticks_per_hour:
            self.current_tick_normalized -= 24 * self.ticks_per_hour
        self.current_hour = round(self.current_tick_normalized / self.ticks_per_hour, 2)

        for a in self.to_add:
            if a is not None:
                # if a.gameid not in self.objects:
                self.objects[a.gameid] = a
                a.on_add()
                HookManager().execute_event("cronus/add", cronus=self, obj=a)
        self.to_add[:] = []

        for o in self.objects.values():
            o.step()

        HookManager().execute_event("cronus/step", cronus=self)

        for d in self.to_delete:
            if d is not None:
                try:
                    d.on_remove()
                    HookManager().execute_event("cronus/remove", cronus=self, obj=d)
                    del(self.objects[d.gameid])
                except:
                    self.logger.exception("general/error")
        self.to_delete[:] = []

    def finalize(self):
        for d in self.objects.values():
            self.remove(d)

        self.to_add[:] = []
        self.step()

    # noinspection PyUnusedLocal
    def get_all(self, data):
        answer = [i.dump() for i in self.objects.values()]
        return {"data": answer}

    def xp(self, killer, killed):
        self.level.xp(killer, killed)

    def mendel(self, data):
        try:
            char = self.objects[data['session']['character_id']]
            char.position.go(data)
            char.on_activity()

            HookManager().execute_event("cronus/mendel", cronus=self, obj=char)
        except:
            self.logger.exception("general/error")

    # noinspection PyUnusedLocal
    def pinus(self, data):
        """
        Private
        INteger
        Universal
        Second
        """
        return {
            "type": "cronus_pinus",
            "tick": self.current_tick,
            "tick_normalized": self.current_tick_normalized,
            "tick_per_hour": self.ticks_per_hour,
            "tick_per_second": self.ticks_per_second
        }

    def distribute_skillpoints(self, data):
        char = self.objects[data['session']['character_id']]
        if 'wtii' in data:
            return {"type": "cronus_suc", "sp": char.skillpoints}
        else:
            return self.level.distribute_skillpoints(char, data)