__author__ = 'crystal'

from core.demeter.hook_manager import HookManager
import physics.persephone


# noinspection PyUnusedLocal
class Persephone:
    def __init__(self):
        self.persephone = physics.persephone.Persephone()
        HookManager().register_hook("cronus/step", "cronus/plugin/persephone", self.step)
        HookManager().register_hook("cronus/add", "cronus/plugin/persephone", self.add)
        HookManager().register_hook("cronus/remove", "cronus/plugin/persephone", self.remove)

    def step(self, cronus=None):
        if not cronus:
            return False

        self.persephone.step(cronus.objects)

    def add(self, cronus=None, obj=None):
        if not obj:
            return False

        self.persephone.add(obj)

    def remove(self, cronus=None, obj=None):
        if not obj:
            return False

        self.persephone.remove(obj)