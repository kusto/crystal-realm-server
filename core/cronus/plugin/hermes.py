__author__ = 'crystal'

import core.hermes.hermes
from core.demeter.hook_manager import HookManager


# noinspection PyUnusedLocal
class Hermes:
    def __init__(self):
        self.hermes = core.hermes.hermes.Hermes()
        HookManager().register_hook("cronus/add", "cronus/plugin/hermes", self.add)
        HookManager().register_hook("cronus/remove", "cronus/plugin/hermes", self.remove)
        HookManager().register_hook("cronus/mendel", "cronus/plugin/hermes", self.mendel)

    def add(self, cronus=None, obj=None):
        if not obj:
            return False

        self.hermes.notify({'event': "cronus_object_add", 'data': obj.dump()}, obj.square)

    def remove(self, cronus=None, obj=None):
        if not obj:
            return False

        self.hermes.notify({'event': "cronus_object_remove", 'data': obj.dump()}, obj.square)

        if obj.type == 1 and obj.client:
            self.hermes.notify_single({"event": "sig_kicked"}, obj.client)
            obj.client.close()

    def mendel(self, cronus=None, obj=None):
        if not obj:
            return False

        nb = {
            "event": "cronus_mendel",
            "pos": obj.position.dump(),
            "gameid": obj.gameid
        }

        self.hermes.notify(nb, obj.square)
        if obj.type == 1 and obj.group:
            self.hermes.notify(nb, obj.group)