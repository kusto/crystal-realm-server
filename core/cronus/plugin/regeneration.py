__author__ = 'crystal'

from core.demeter.hook_manager import HookManager
import time


class Regeneration:
    def __init__(self):
        HookManager().register_hook("cronus/pc/step", "cronus/plugin/regeneration", self.regen)
        HookManager().register_hook("cronus/enemy/step", "cronus/plugin/regeneration", self.regen)

    # noinspection PyMethodMayBeStatic
    def regen(self, character=None):
        if not character:
            return False

        if not character.state.alive():
            return False

        flag = False

        if 0 < character.hp < character.stats.max_hp:
            character.hp += character.stats.hp_regen
            flag = True

        if character.mp < character.stats.max_mp:
            character.mp += character.stats.mp_regen
            flag = True

        if flag:
            HookManager().execute_event("cronus/state-change", obj=character)

    # noinspection PyMethodMayBeStatic
    def activity(self, character=None):
        character.last = time.time()
        character.state.offline = False