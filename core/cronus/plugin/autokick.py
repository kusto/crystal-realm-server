__author__ = 'crystal'

from core.demeter.hook_manager import HookManager
import time


class Autokick:
    def __init__(self):
        HookManager().register_hook("cronus/pc/step", "cronus/plugin/autokick", self.kick)
        HookManager().register_hook("cronus/pc/activity", "cronus/plugin/autokick", self.activity)

    # noinspection PyMethodMayBeStatic
    def kick(self, character=None):
        if not character:
            return False

        if not character.state.offline and time.time() - character.last > 120:
            character.state.offline = True

        if character.state.offline and time.time() - character.last > 300:
            character.remove()

    # noinspection PyMethodMayBeStatic
    def activity(self, character=None):
        character.last = time.time()
        character.state.offline = False