__author__ = 'crystal'

from . import basemodel
from peewee import *


class StaticClass:
    def __init__(self):
        self.size = ""
        pass

    def fromdb(self, model):
        self.size = model.size

        return self


class StaticClassDB(basemodel.BaseModel):
    size = CharField()

    class Meta:
        db_table = 'static_class'

