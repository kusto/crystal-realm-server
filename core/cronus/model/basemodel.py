__author__ = 'crystal'

from peewee import *
from core.uranus.uranus import Uranus


class BaseModel(Model):
    class Meta:
        database = Uranus().get_connection()
