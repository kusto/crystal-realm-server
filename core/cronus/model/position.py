__author__ = 'crystal'

import json
# noinspection PyUnresolvedReferences
import logging
# noinspection PyUnresolvedReferences
from core.hermes.hermes import Hermes


# noinspection PyBroadException
class Position:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0
        self.to_x = 0
        self.to_y = 0
        self.to_z = 0
        self.angle = 0
        self.v_x = 0
        self.v_y = 0
        self.v_z = 0
        self.max_speed = 0

    def dump(self):
        return {
            'x': round(self.x, 2),
            'y': round(self.y, 2),
            'z': round(self.z, 2),
            'to_x': round(self.to_x, 2),
            'to_y': round(self.to_y, 2),
            'to_z': round(self.to_z, 2),
            'max_speed': self.max_speed,
            'angle': self.angle
        }

    def save(self):
        return json.dumps({
            'x': round(self.x, 2),
            'y': round(self.y, 2),
            'z': round(self.z, 2)
        })

    def load(self, string):
        try:
            obj = json.loads(string)
            if 'new' in obj:
                self.x = 0
                self.y = 0
                self.z = 0
            else:
                self.x = obj['x']
                self.y = obj['y']
                self.z = obj['z']
            self.to_x = self.x
            self.to_y = self.y
            self.to_z = self.z
        except:
            pass

    def go(self, data):
        if data['x']:
            self.to_x = data['x']

        if data['y']:
            self.to_y = data['y']

        if data['z']:
            self.to_z = data['z']