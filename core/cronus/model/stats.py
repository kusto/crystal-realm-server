__author__ = 'crystal'

import json
# noinspection PyUnresolvedReferences
import logging
import math


# noinspection PyBroadException,PyPep8
class Stats:
    def __init__(self):
        self.str = 0
        self.agi = 0
        self.int = 0
        self.luc = 0

        self.pdm = 0
        self.mdm = 0
        self.prk = 0
        self.mrk = 0

        self.gdm = 0
        self.grk = 0

        self.max_hp = 0
        self.max_mp = 0
        self.hp_regen = 0
        self.mp_regen = 0

        self.max_speed = 0

        self.recalculate()

    def dump(self):
        return {
            'strength': self.str,
            'agility': self.agi,
            'intelligence': self.int,
            'luck': self.luc
        }

    def save(self):
        obj = {
            'str': self.str,
            'agi': self.agi,
            'int': self.int,
            'luc': self.luc
        }

        return json.dumps(obj)

    def load(self, string):
        try:
            obj = json.loads(string)
            if 'new' in obj:
                self.str = 0
                self.agi = 0
                self.int = 0
                self.luc = 0
            else:
                self.str = obj['str']
                self.agi = obj['agi']
                self.int = obj['int']
                self.luc = obj['luc']
        except:
            pass

        self.recalculate()

    def get_max_hp(self):
        return 100 + self.str * 20

    def get_max_mp(self):
        return self.int * 30

    def recalculate(self):

        """
        Physics Damage Multiplicator - мультипликатор физических повреждений
        """
        self.pdm = 1 \
                   + math.sqrt(self.agi) / 10 \
                   + math.pow(self.str, 0.25)

        """
        Magic Damage Multiplicator - мультипликатор магических повреждений
        """
        self.mdm = 1 \
                   + math.sqrt(self.int) / 8

        """
        Physical Resist Koef - коэффицент сопротивления физическому урону
        """
        self.prk = 1 \
                   + self.agi / 100

        """
        Magical Resist Koef - коэффицент сопротивления магическому урону
        """
        self.mrk = 1.5 \
                   + math.sqrt(self.int) / 20

        """
        General Damage Multiplicator - общий мультипликатор урона
        """
        self.gdm = math.sqrt(1 + self.luc / 80)

        """
        General Resist Koef - общий коэфицент сопротивления урону
        """
        self.grk = math.sqrt(
            1 + 3 * math.sqrt(self.luc) / 10
        )

        self.max_hp = 20 * self.str + 100
        self.max_mp = 30 * self.int
        self.max_speed = 0.1 * self.agi + 5
        self.hp_regen = ( 5 + 3 * math.sqrt(self.str) ) / 20
        self.mp_regen = self.int / 20

    def get_real_damage_sent(self, damage, typ):
        if typ == "clear":
            return damage

        damage *= self.gdm

        if typ == "phys":
            damage *= self.pdm
        if typ == "mag":
            damage *= self.mdm

        return damage

    def get_real_damage_got(self, damage, typ):
        if typ == "clear":
            return damage

        damage /= self.grk

        if typ == "phys":
            damage /= self.prk
        if typ == "mag":
            damage /= self.mrk

        return damage