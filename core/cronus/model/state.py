__author__ = 'crystal'

import time
# import logging


class State:
    def __init__(self):
        # 1 - мертв
        # 2 - возрождение
        # 3 = в бою
        # 0 - нормальное (не в бою, жив, онлайн)
        self.state = 0

        # Время последнего обновления состояния
        self.lastupdate = 0

        # Заморозка состояния
        # Если флаг взведен - состояние не будет меняться с течением времени
        self.freeze = 0

        self.offline = False

    def dump(self):
        return {
            'state': self.state,
            'last': self.lastupdate,
            'freeze': self.freeze
        }

    def update(self, state, comment=""):
        if self.freeze:
            pass
        else:
            self.state = state
            self.lastupdate = int(time.time())

    def alive(self):
        if self.state == 1 or self.state == 2:
            return False
        else:
            return True

    def step(self):
        pass