__author__ = 'crystal'

from . import basemodel
from peewee import *


class NPCClass:
    def __init__(self):
        self.id = 0
        self.hp = 0
        self.mp = 0
        self.max_hp = 0
        self.max_mp = 0
        self.level = 0
        self.name = "Безымянный класс"
        self.db = None
        self.gameid = "C_NPC_nameless"

    def fromdb(self, model):
        self.db = model
        self.id = model.id
        self.hp = model.hp
        self.mp = model.mp
        self.max_hp = model.max_hp
        self.max_mp = model.max_mp
        self.level = model.level
        self.name = model.name
        self.gameid = model.gameid

        return self


class NPCClassDB(basemodel.BaseModel):
    id = IntegerField()
    gameid = CharField()
    hp = IntegerField()
    level = IntegerField()
    mp = IntegerField()
    max_hp = IntegerField()
    max_mp = IntegerField()
    name = CharField()

    class Meta:
        db_table = 'npc_classes'
