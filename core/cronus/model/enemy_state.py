__author__ = 'crystal'

from . import state
import time
from core.hermes.hermes import Hermes


class EnemyState(state.State):
    def __init__(self, parent):
        super().__init__()
        self.respawn = 0

        self.parent = parent

        self.lastupdate = time.time()

    def update(self, ns, comment=""):
        super().update(ns, comment)
        self.parent.on_state_change(ns)
        Hermes().notify({'event': "hermes_cronus_state_change", 'data': self.parent.dump()}, self.parent.square)

    def step(self):
        if self.state == 1 and self.lastupdate + self.respawn <= time.time():
            self.update(2, self.parent.gameid)

        if self.state == 2 and self.lastupdate + self.respawn <= time.time():
            self.update(0, self.parent.gameid)