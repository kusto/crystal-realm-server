__author__ = 'crystal'

import logging
from peewee import *
from . import entity, position, basemodel


class Static(entity.Entity):
    def __init__(self, cronus=None):
        super().__init__(cronus=cronus)
        self.gameid = ""
        self.position = position.Position()
        self.type = 5

        self.square = None
        self.location = None

        self.id = 0

    def dump(self):
        return {
            'type': self.type,
            'pos': self.position.dump(),
            'id': self.gameid
        }

    def on_add(self):
        logging.debug("Добавляю статичный объект #"+self.gameid)

    def on_remove(self):
        logging.debug("Удаляю статичный объект #"+self.gameid)

    def fromdb(self, model):
        try:
            # noinspection PyUnusedLocal
            my_class = self.cronus.static_classes[model.classid]
        except Exception as e:
            logging.debug("Определение класса повреждено у "+model.gameid+": "+e.__str__())

        self.id = model.id

        self.gameid = model.gameid
        self.position.load(model.position)

        self.square = self.cronus.lm.get_square(self.position.x, self.position.y)
        self.location = self.cronus.lm.get_location(self.square)

        return self


class StaticDB(basemodel.BaseModel):
    id = IntegerField()
    gameid = CharField()
    classid = IntegerField()
    position = CharField()

    class Meta:
        db_table = 'static'