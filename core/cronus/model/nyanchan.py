__author__ = 'crystal'

from . import character
import logging
import time
# noinspection PyUnresolvedReferences
from core.ares.ares import Ares


class NyanChan(character.Character):
    def __init__(self, i=0, parent=None):
        super().__init__(i)
        self.type = 4
        self.parent = parent
        self.last = time.time()
        self.gameid = "Nyan-Chan"

        self.square = (0, 0)
        self.location = self.parent.lm.get_location(self.square)

    def on_add(self):
        logging.debug("Здесь я стою, и именем мне будет вечность.")

    def on_remove(self):
        logging.debug("Я умираю. Значит гибнет мир...")

    def step(self):
        """

        try:
            if time.time() - self.last > 7:
                self.last = time.time()
                Ares().skills['S_assrape'].use(self, self.parent.objects['enemy_mr_sucker'])
        except Exception as e:
            logging.debug(e.__str__())
        """
        pass