__author__ = 'crystal'

from . import basemodel, stats
from peewee import *


class EnemyClass:
    def __init__(self):
        self.level = 0
        self.name = ""
        self.db = None
        self.gameid = "C_NPC_nameless"
        self.respawn = 0
        self.stats = stats.Stats()

    def fromdb(self, model):
        self.db = model
        self.level = model.level
        self.name = model.name
        self.gameid = model.gameid
        self.respawn = model.respawn

        self.stats.load(model.stats)

        return self


class EnemyClassDB(basemodel.BaseModel):
    gameid = CharField()
    stats = CharField()
    hp = IntegerField()
    level = IntegerField()
    mp = IntegerField()
    name = CharField()
    respawn = IntegerField()

    class Meta:
        db_table = 'enemy_classes'

