__author__ = 'crystal'

from . import character, basemodel
from peewee import *
import logging


# noinspection PyBroadException
class NPCharacter(character.Character):
    def __init__(self, i=0, cronus=None):
        super().__init__(i=i, cronus=cronus)
        self.classid = 0
        self.classstring = ""

        self.type = 2

        self.db = None

        self.name = ""

        self.max_hp = 0
        self.max_mp = 0

        self.square = None
        self.location = None

    def fromdb(self, model):
        self.db = model
        # noinspection PyUnusedLocal
        try:
            my_class = self.cronus.classes[model.classid]
            self.hp = my_class.hp
            self.mp = my_class.mp
            self.max_hp = my_class.max_hp
            self.max_mp = my_class.max_mp
            self.level = my_class.level
            self.name = my_class.name
            self.classstring = my_class.gameid
        except Exception as e:
            logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                .debug("cronus/model/npc/db-class-error", {'id': self.gameid})
        self.id = model.id
        self.gameid = model.gameid
        self.classid = model.classid

        self.db = model

        if model.hp:
            self.hp = model.hp

        if model.mp:
            self.mp = model.mp

        if model.max_hp:
            self.max_hp = model.max_hp

        if model.max_mp:
            self.max_mp = model.max_mp

        if model.level:
            self.level = model.level

        if model.name:
            self.name = model.name

        self.position.load(model.position)

        self.square = self.cronus.lm.get_square(self.position.x, self.position.y)
        self.location = self.cronus.lm.get_location(self.square)

        return self

    def dump(self):
        tmp = super().dump()
        tmp['max_hp'] = self.max_hp
        tmp['max_mp'] = self.max_mp
        tmp['classid'] = self.classstring
        return tmp


class NPCharacterDB(basemodel.BaseModel):
    classid = IntegerField()
    enabled = IntegerField()
    gameid = CharField()
    hp = IntegerField()
    level = IntegerField()
    mp = IntegerField()
    max_hp = IntegerField()
    max_mp = IntegerField()
    name = CharField()
    position = CharField()
    state = CharField()

    class Meta:
        db_table = 'npc'
