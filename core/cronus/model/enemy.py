__author__ = 'crystal'

from . import character, basemodel, enemy_state, stats
from peewee import *
import logging
from physics.persephone import Persephone
# noinspection PyUnresolvedReferences
from core.ares.ares import Ares
from core.demeter.hook_manager import HookManager


# noinspection PyBroadException
class EnemyCharacter(character.Character):
    def __init__(self, i=0, cronus=None):
        super().__init__(i=i, cronus=cronus)
        self.type = 3
        self.skills = []

        self.classid = 0
        self.classstr = ""

        self.state = enemy_state.EnemyState(parent=self)

        self.stats = None

        self.square = None
        self.location = None

        self.interactable = True

        self.lastdd = None
        self.lastvictim = None

    def fromdb(self, model):
        try:
            my_class = self.cronus.enemy_classes[model.classid]
            self.level = my_class.level
            self.name = my_class.name
            self.state.respawn = my_class.respawn

            self.stats = my_class.stats

            self.classstr = my_class.gameid
        except:
            logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                .debug("cronus/model/enemy/db-class-error", {'id': self.gameid})

            self.stats = stats.Stats()

        self.id = model.id
        self.gameid = model.gameid
        self.classid = model.classid

        self.hp = self.stats.max_hp
        self.mp = self.stats.max_mp

        if model.level:
            self.level = model.level

        if model.name:
            self.name = model.name

        if model.respawn:
            self.state.respawn = model.respawn

        self.position.load(model.position)

        self.position.max_speed = self.stats.max_speed

        self.square = self.cronus.lm.get_square(self.position.x, self.position.y)
        self.location = self.cronus.lm.get_location(self.square)

        return self

    def step(self):
        super().step()

        HookManager().execute_event("cronus/enemy/step", character=self)

        # Ares().process_effects(self)

        if self.hp <= 0 and self.state.alive():
            self.state.update(1, self.gameid)
        self.state.step()

    def dump(self):
        tmp = super().dump()
        tmp['max_hp'] = self.stats.max_hp
        tmp['max_mp'] = self.stats.max_mp
        tmp['classid'] = self.classstr
        return tmp

    def on_state_change(self, state):
        super().on_state_change(state)
        if state == 2:
            pass

        if state == 0:
            # self.hp = self.stats.get_max_hp()
            # self.mp = self.stats.get_max_mp()
            self.hp = self.stats.max_hp
            self.mp = self.stats.max_mp
            Persephone().on(self)

        if state == 1:
            self.on_death(self.lastdd)
            Persephone().off(self)

    def on_damage(self, dd):
        self.lastdd = dd

    def on_damage_sent(self, tank):
        self.lastvictim = tank

    def on_death(self, slayer):
        # ToDo: считать убийцей не того, кто сделал ластхит, а того, кто наверху аггро-таблицы
        # ToDo: дроп
        if slayer:
            slayer.on_kill(self)

    def on_kill(self, killed):
        pass


class EnemyDB(basemodel.BaseModel):
    classid = IntegerField()
    gameid = CharField()
    hp = IntegerField()
    level = IntegerField()
    mp = IntegerField()
    name = CharField()
    position = CharField()
    respawn = IntegerField()
    state = CharField()

    class Meta:
        db_table = 'enemy'