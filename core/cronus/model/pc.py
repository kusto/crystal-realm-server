__author__ = 'crystal'

from . import character, basemodel, player_state
from peewee import *
# noinspection PyUnresolvedReferences
import logging
# noinspection PyUnresolvedReferences
from core.ares.ares import Ares
from core.hermes.hermes import Hermes
from physics.persephone import Persephone
import time
from core.hecate.hecate import Hecate
from core.demeter.hook_manager import HookManager


# noinspection PyBroadException
class PlayerCharacter(character.Character):
    def __init__(self, i=0, cronus=None):
        super().__init__(i=i, cronus=cronus)
        self.type = 1
        # self.skills = {}

        self.classid = 0

        self.skillpoints = 0

        self.state = player_state.PlayerState(parent=self)

        self.square = None
        self.location = None

        self.hermes = Hermes()

        self.db = None

        self.client = None

        self.pk_mode = 0
        self.pk_flag = False

        self.last = time.time()

        self.nhk_talk = None

        self.lastdd = None
        self.lastvictim = None

    def fromdb(self, model):
        self.id = model.id
        self.gameid = "player_"+str(model.id)
        self.name = model.name

        self.level = model.level
        self.xp = model.xp
        self.skillpoints = model.skillpoints

        self.position.load(model.position)

        self.stats.load(model.stats)
        self.hp = self.stats.max_hp
        self.mp = self.stats.max_mp

        self.state.respawn = 15 + self.level * 10

        self.position.max_speed = self.stats.max_speed

        # self.skills = Ares()._get_skills(self.id)

        self.db = model

        self.interactable = True

        return self

    def save(self):
        self.db.name = self.name

        self.db.level = self.level
        self.db.xp = self.xp

        self.db.position = self.position.save()
        self.db.stats = self.stats.save()

        self.db.skillpoints = self.skillpoints

        self.db.save()

    def rebind(self, new, old=None):
        try:
            if old:
                area = self.cronus.lm.get_area(old[0], old[1])
                for k in area:
                    self.hermes.remove_listener(k, self.client)

            area = self.cronus.lm.get_area(new[0], new[1])
            for k in area:
                self.hermes.add_listener(k, self.client)
        except:
            logging.getLogger(__name__ + '.' + self.__class__.__name__) \
                .exception("general/error")

    def step(self):
        super().step()

        HookManager().execute_event("cronus/pc/step", character=self)
        # Ares().process_effects(self)

        if self.hp <= 0 and self.state.alive():
            self.hp = 0
            self.state.update(1, self.gameid)

        self.state.step()

    def resurrection_by_erection(self):
        pass

    def dump(self):
        tmp = super().dump()
        tmp['max_hp'] = self.stats.max_hp
        tmp['max_mp'] = self.stats.max_mp
        tmp['square'] = str(self.square[0])+" "+str(self.square[1])
        tmp['location'] = self.location.name
        return tmp

    def on_state_change(self, state):
        super().on_state_change(state)
        if state == 2:
            rp = self.location.get_respawn()
            Persephone().goto(self, rp['x'], rp['y'], rp['z'])

        if state == 0:
            self.hp = self.stats.get_max_hp()
            self.mp = self.stats.get_max_mp()
            Persephone().on(self)

        if state == 1:
            self.on_death(self.lastdd)
            Persephone().off(self)

    def on_add(self):
        super().on_add()

        self.reload()

    def reload(self):
        self.square = self.cronus.lm.get_square(self.position.x, self.position.y)
        self.location = self.cronus.lm.get_location(self.square)
        self.rebind(self.square)

        Hecate().build_cache_entries(self.id)

    def on_remove(self):
        super().on_remove()

        Hecate().clear_cache_entries(self.id)

        self.save()

        if self.group:
            self.group.remove(self)

    def on_activity(self):
        HookManager().execute_event("cronus/pc/activity", character=self)

    def is_friendly(self, target):
        if super().is_friendly(target):
            return True

        if target.type == 1 and not self.pk_mode and not target.pk_flag:
            return True

        if self.group is not None and target.gameid in self.group.members:
            return True

        return False

    def on_damage(self, dd):
        self.lastdd = dd

    def on_damage_sent(self, tank):
        # ToDo: флаг убийцы; подумать об очках кармы за ПК
        self.lastvictim = tank

    def on_death(self, slayer):
        if slayer:
            slayer.on_kill(self)

    def on_kill(self, killed):
        if killed:
            self.cronus.xp(self, killed)
            for q in Hecate().user_entries[self.id]:
                Hecate().user_entries[self.id][q].on_kill(killed)

        Hecate().refresh_quests(self)

    def add_xp(self, xp):
        self.xp += xp

        if self.xp >= (2 ** (self.level - 1)) * 1000:
            self.xp -= (2 ** (self.level - 1)) * 1000

            self.level += 1

            self.levelup()

            self.save()

        else:
            Hermes().notify_single({"event": "cronus_xp", "xp": self.xp}, self.client)

    def levelup(self):
        self.skillpoints += 5

        Hermes().notify_single(
            {
                "event": "cronus_xp",
                "level": self.level,
                "xp": self.xp,
                "skillpoints": self.skillpoints
            }, self.client)


class PlayerDB(basemodel.BaseModel):
    id = IntegerField()
    name = CharField()

    level = IntegerField()
    xp = IntegerField()
    skillpoints = IntegerField()

    position = CharField()
    stats = CharField()

    account = IntegerField()
    statuscode = IntegerField()

    class Meta:
        db_table = 'player'
