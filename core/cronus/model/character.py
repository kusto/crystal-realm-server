__author__ = 'crystal'

import logging
from . import state, stats, entity, position
from core.demeter.hook_manager import HookManager


class Character(entity.Entity):
    def __init__(self, i=0, cronus=None):
        super().__init__(cronus=cronus)
        self.id = i
        self.gameid = ""
        self.name = ""
        self.hp = 0
        self.mp = 0
        self.level = 0
        self.xp = 0
        self.state = state.State()
        self.effects = []
        self.stats = stats.Stats()
        self.position = position.Position()

        self.interactable = False

        self.group = None

    def dump(self):
        eff = []
        for effect in self.effects:
            e = effect['effect'].dump()
            e.update({'set': int(effect['first'])})
            eff.append(e)
        return {
            'hp': self.hp,
            'mp': self.mp,
            'level': self.level,
            'stats': self.stats.dump(),
            'state': self.state.dump(),
            'type': self.type,
            'pos': self.position.dump(),
            'name': self.name,
            'id': self.gameid,
            'effects': eff
        }

    def step(self):
        super().step()

        HookManager().execute_event("cronus/character/step", character=self)

    def on_add(self):
        logging.getLogger(__name__ + '.' + self.__class__.__name__) \
            .debug("cronus/model/character/spawn", {'id': self.gameid})

    def on_remove(self):
        logging.getLogger(__name__ + '.' + self.__class__.__name__) \
            .debug("cronus/model/character/remove", {'id': self.gameid})

    def on_damage(self, dd):
        pass

    def on_damage_sent(self, tank):
        pass

    def on_kill(self, killed):
        pass

    def on_death(self, slayer):
        pass

    def on_state_change(self, ns):
        pass

    def resurrection_by_erection(self):
        pass

    def remove(self):
        self.cronus.remove(self)

    def is_interactable(self):
        return self.interactable

    def is_friendly(self, target):
        if target.gameid == self.gameid:
            return True

        if target.type == 2 or target.type == 4 or target.type == 5:
            return True

        return False