class GenericStorage:
    storage = None
    default_value = None

    def __getitem__(self, item):
        return self.get(item)

    def __setitem__(self, item, value):
        return self.set(item, value)

    def __delitem__(self, item):
        return self.unset(item)

    def __contains__(self, item):
        return self.isset(item)

    def set(self, item, value):
        raise ImportError("Переопределите метод set в " + self.__class__.__name__)

    def get(self, item):
        raise ImportError("Переопределите метод get в " + self.__class__.__name__)

    def isset(self, item):
        raise ImportError("Переопределите метод isset в " + self.__class__.__name__)

    def unset(self, item):
        raise ImportError("Переопределите метод unset в " + self.__class__.__name__)

    def set_storage(self, storage):
        self.storage = storage
        
    def get_storage(self):
        return self.storage

    def get_default_value(self):
        return self.default_value


class DictStorage(GenericStorage):
    def get(self, item):
        if self.isset(item):
            return self.storage[item]
        else:
            return self.get_default_value()

    def set(self, item, value):
        self.storage[item] = value

    def isset(self, item):
        return item in self.storage

    def unset(self, item):
        if self.isset(item):
            del(self.storage[item])


class RecursiveDictStorage(GenericStorage):
    separator = "_"
    
    def tokenize(self, string):
        lst = string.split(self.separator)
        lst.reverse()
        return lst

    def get(self, item):
        return self._get(self.get_storage(), self.tokenize(item))

    def set(self, item, value):
        return self._set(self.get_storage(), self.tokenize(item), value)

    def isset(self, item):
        return self._isset(self.get_storage(), self.tokenize(item))

    def unset(self, item):
        return self._unset(self.get_storage(), self.tokenize(item))

    def _get(self, box, tail):
        elem = tail.pop()
        
        if elem not in box:
            return self.get_default_value()

        if tail:
            return self._get(box[elem], tail)
        else:
            return box[elem]

    def _set(self, box, tail, value):
        elem = tail.pop()
        
        if elem not in box:
            box[elem] = {}

        if tail:
            if isinstance(box[elem], dict):
                return self._set(box[elem], tail, value)
            else:
                return False
        else:
            box[elem] = value
            return True

    def _isset(self, box, tail):
        elem = tail.pop()
        
        if elem not in box:
            return False

        if tail:
            return self._isset(box[elem], tail)
        else:
            return True

    def _unset(self, box, tail):
        elem = tail.pop()
        
        if elem not in box:
            return True

        if tail:
            return self._unset(box[elem], tail)
        else:
            del(box[elem])
            return True
