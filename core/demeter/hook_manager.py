"""
@ToDo: найти цитату
"""

import logging

from core.demeter.singleton import Singleton


# noinspection PyBroadException,PyBroadException
class HookManager(metaclass=Singleton):
    def __init__(self):
        self.hooks = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def register_event(self, event):
        if event not in self.hooks:
            self.hooks[event] = {}

    def execute_event(self, event, *args, **kwargs):
        if event not in self.hooks:
            return None
        
        results = {}

        for bind in self.hooks[event]:
            try:
                results[bind] = self.hooks[event][bind](*args, **kwargs)
            except:
                self.logger.exception("demeter/hook/run-error", {'name': bind, 'event': event})

        return results

    def register_hook(self, event, name, hook):
        if not isinstance(name, str):
            self.logger.debug("demeter/hook/nake-string-only")
            return False

        if event not in self.hooks:
            self.register_event(event)

        if name in self.hooks[event]:
            self.logger.warning("demeter/hook/double-hook", {'name': name, 'event': event})
            return False

        self.hooks[event][name] = hook

    def run_single_hook(self, event, hook, *args, **kwargs):
        if event not in self.hooks:
            self.logger.debug("demeter/hook/event-404", {'event': event})
            return False

        if hook not in self.hooks[event]:
            self.logger.debug("demeter/hook/hook-404", {'name': hook, 'event': event})
            return False

        try:
            return self.hooks[event][hook](*args, **kwargs)
        except:
            self.logger.exception("demeter/hook/run-error", {'name': hook, 'event': event})
            return False
