__author__ = 'crystal'

from .chain import ChainProcessorManager


class GenericCompressor:
    name = "GenericCompressor"
    id_prefix = 'compressor/generic/'

    def __init__(self):
        ChainProcessorManager().build_processor(self.id_prefix + "compress", self.compress)
        ChainProcessorManager().build_processor(self.id_prefix + "decompress", self.decompress)

    def compress(self, data):
        pass

    def decompress(self, data):
        pass


class IdleCompressor(GenericCompressor):
    name = "IdleCompressor"
    id_prefix = "compressor/idle/"

    def compress(self, data):
        return data

    def decompress(self, data):
        return data


IdleCompressor()

try:
    # noinspection PyUnresolvedReferences
    import zlib

    class GzipCompressor(GenericCompressor):
        name = "GZipCompressor"
        id_prefix = "compressor/gzip/"

        def compress(self, data):
            return zlib.compress(data)

        def decompress(self, data):
            return zlib.decompress(data)

    GzipCompressor()

except ImportError:
    pass

try:
    # noinspection PyUnresolvedReferences
    import lzma

    class LzmaCompressor(GenericCompressor):
        name = "LzmaCompressor"
        id_prefix = "compressor/lzma/"

        def compress(self, data):
            return lzma.compress(data)

        def decompress(self, data):
            return lzma.decompress(data)

    LzmaCompressor()

except ImportError:
    pass