"""
Вставай, проклятьем заклеймённый,
Голодный, угнетённый люд!
Наш разум — кратер раскалённый,
Потоки лавы мир зальют.
Сбивая прошлого оковы,
Рабы восстанут, а затем
Мир будет изменён в основе:
Теперь ничто — мы станем всем!

Никто не даст нам избавленья:
Ни бог, ни царь и ни герой.
Добьёмся мы освобожденья
Своею собственной рукой.
Чтоб вор вернул нам всё, что взял он,
Чтоб дух тюрьмы навек пропал,
Ковать железо будем с жаром,
Пока горяч ещё металл.

Держава — гнёт, закон лишь маска,
Налоги душат невтерпёж.
Никто богатым не указка,
И прав у бедных не найдёшь.
Довольно государства, право,
Услышьте Равенства завет:
Отныне есть у нас лишь право,
Законов же у равных нет!

Дошли в корысти до предела
Монархи угля, рельс и руд.
Их омерзительное дело —
Лишь угнетать и грабить Труд.
Мы создаём все капиталы,
Что в сейфах подлецов лежат.
Вперед! Теперь пора настала
Своё потребовать назад!
Своё потребовать назад!
Своё потребовать назад!
"""

import logging

import core.cerberus.cerberus
import core.pandora.pandora
import core.ariadne.ariadne
import core.demeter.logging
import core.cronus.cronus
import core.hermes.hermes
import core.athena.athena
import core.hecate.hecate
import core.uranus.uranus
import physics.persephone
import core.charon.charon
import core.hades.hades
import core.demeter.singleton
import core.ares.ares
import core.zeus.zeus
import core.nhk.nhk
from .generic_server import GenericServerHost
from .i18n import I18n


class Loader(metaclass=core.demeter.singleton.Singleton):
    def __init__(self):
        self.dragon = None
        self.network = None
        self.pandora = None
        self.ariadne = None
        self.cronus = None
        self.hermes = None
        self.hades = None
        self.ares = None
        self.zeus = None
        self.GameServer = None
        self.ChatServer = None
        self.Cerberus = None
        self.persephone = None
        self.athena = None
        self.nhk = None
        self.hecate = None
        self.uranus = None
        self.i18n = None

    def load(self):
        print("  ___             _        _   ___          _")
        print(" / __|_ _ _  _ __| |_ __ _| | | _ \___ __ _| |_ __")
        print("| (__| '_| || (_-<  _/ _` | | |   / -_) _` | | '  \\")
        print(" \___|_|  \_, /__/\__\__,_|_| |_|_\___\__,_|_|_|_|_|")
        print("          |__/")

        print("Начинаю инициализацию сервера.\nЭтот процесс может занять некоторое время...")

        core.demeter.logging.Logging.load()
        logging.info("Система первичного логгирования запущена")

        self.i18n = I18n()

        self.pandora = core.pandora.pandora.Pandora()

        self.uranus = core.uranus.uranus.Uranus()

        self.ariadne = core.ariadne.ariadne.Ariadne()

        self.network = core.charon.charon.Charon()

        self.hermes = core.hermes.hermes.Hermes()

        self.athena = core.athena.athena.Athena()
        self.athena.load()

        self.hades = core.hades.hades.Hades()
        self.hades.load()

        self.Cerberus = core.cerberus.cerberus.Cerberus()
        self.Cerberus.load()
        self.ariadne.enable_cerberus(self.Cerberus)

        self.ares = core.ares.ares.Ares()
        self.ares.load()

        self.persephone = physics.persephone.Persephone()
        self.persephone.load()

        self.hecate = core.hecate.hecate.Hecate()
        self.hecate.load()

        self.cronus = core.cronus.cronus.Cronus()
        self.cronus.load()

        self.ares.cronus = self.cronus

        self.zeus = core.zeus.zeus.Zeus()

        self.nhk = core.nhk.nhk.NHK()
        self.nhk.load()

        self.athena.logic.hecate = self.hecate
        self.athena.logic.cronus = self.cronus

        self.network.load()

        logging.info(self.i18n('demeter/loader/server-init-ok'))

    def start(self):
        GenericServerHost().startup()
        # noinspection PyCallingNonCallable
        logging.info(self.i18n('demeter/loader/server-start-ok'))

    def stop(self):
        GenericServerHost().shutdown()
        # noinspection PyCallingNonCallable
        logging.info(self.i18n('demeter/loader/server-stop-ok'))