__author__ = 'crystal'

from .chain import ChainProcessorManager


class GenericCrypter:
    name = "GenericCrypter"
    id_prefix = 'crypter/generic/'

    def __init__(self):
        ChainProcessorManager().build_processor(self.id_prefix + "encrypt", self.encrypt)
        ChainProcessorManager().build_processor(self.id_prefix + "decrypt", self.decrypt)

    def encrypt(self, data):
        pass

    def decrypt(self, data):
        pass


class IdleCrypter(GenericCrypter):
    name = "IdleCrypter"
    id_prefix = 'crypter/idle/'

    def encrypt(self, data):
        return data

    def decrypt(self, data):
        return data
