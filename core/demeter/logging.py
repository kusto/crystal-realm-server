from .i18n import I18n
import logging
from .singleton import Singleton
import sys


class LoggingFilterRemover(logging.Filter):
    def filter(self, record):
        return False


class LoggingFilterI18n(logging.Filter):
    def filter(self, record):
        if record.args:
            record.msg = I18n()(record.msg, **record.args)
        else:
            record.msg = I18n()(record.msg)
        return True


class Logging(logging.Logger, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def load():
        sh = logging.StreamHandler(sys.stderr)
        sh.setFormatter(logging.Formatter('[%(asctime)s] %(filename)20s:%(lineno)4d %(levelname)8s  %(message)s'))
        sh.addFilter(LoggingFilterI18n())
        logging.getLogger('peewee').addFilter(LoggingFilterRemover())
        logging.getLogger('core').addHandler(sh)
        logging.getLogger('core').propagate = False
        logging.basicConfig(
            format='[%(asctime)s] %(filename)20s:%(lineno)4d %(levelname)8s  %(message)s',
            level=logging.DEBUG
        )

    def debug(self, string, *args, **kwargs):
        return super(self.__class__, self).debug(I18n()(string, *args, **kwargs))

    def warning(self, string, *args, **kwargs):
        return super(self.__class__, self).warning(I18n()(string, *args, **kwargs))

    def error(self, string, *args, **kwargs):
        return super(self.__class__, self).error(I18n()(string, *args, **kwargs))

    def info(self, string, *args, **kwargs):
        return super(self.__class__, self).info(I18n()(string, *args, **kwargs))

    def exception(self, string, *args, **kwargs):
        return super(self.__class__, self).exception(I18n()(string, *args, **kwargs))


class LoggingManager:
    def __init__(self):
        # 1. Для обратной совместимости:

        logging.basicConfig(
            format='[%(asctime)s] %(filename)15s:%(lineno)4d %(levelname)8s  %(message)s',
            level=logging.DEBUG
        )

        # 2. Потому, что пошло оно

        logging.getLogger('peewee').addFilter(LoggingFilterRemover())