__author__ = 'crystal'

import queue
import threading
import time


class Task:
    def __init__(self):
        self.func = None
        self.args = []
        self.kwargs = {}
        self.callback = None
        # self.condition = None

    def set_func(self, func):
        self.func = func

        return self

    def set_context(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

        return self

    def set_callback(self, callback):
        self.callback = callback

        return self

    # noinspection PyCallingNonCallable
    def execute(self):
        result = self.func(*self.args, **self.kwargs)
        self.callback(result, *self.args, **self.kwargs)


class TaskWorker:
    def __init__(self, q):
        self.queue = q
        self._stopped = False
        self.thread = None

    def loop(self):
        while not self._stopped:
            if self.queue.empty():
                time.sleep(0.1)
            else:
                task = self.queue.get()
                if task:
                    task.execute()

    def start(self):
        self.thread = threading.Thread(target=self.loop)
        self.thread.start()
        return self

    def stop(self):
        self._stopped = True
        self.thread.join()


# noinspection PyShadowingNames
class TaskManager:
    def __init__(self, max_workers=5):
        self.MAX_WORKERS = max_workers
        self.queue = queue.Queue()
        # noinspection PyUnusedLocal
        self.workers = [TaskWorker(self.queue).start() for i in range(0, self.MAX_WORKERS)]

    def execute(self, task):
        self.queue.put(task)

    def shutdown(self):
        for i in self.workers:
            i.stop()


if __name__ == "__main__":
    tm = TaskManager(5)

    def cb(result):
        print(result)

    def gc(num):

        def fn():
            time.sleep(4)
            return num

        return fn

    for i in range(10):
        ts = Task().set_func(gc(i)).set_callback(cb)
        tm.execute(ts)

    time.sleep(10)
    tm.shutdown()
