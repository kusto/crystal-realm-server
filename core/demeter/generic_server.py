__author__ = 'crystal'

"""
Он замолк. И дикий пламень
Вмиг потух в его очах,
А рука вождя, как камень,
Замерла в его руках.
Но безжизнен мавр на месте,
Он не свел с врагов лица,
И усмешка злобной мести
На устах у мертвеца.

Опустела Альпухара,
Груда камней, груда тел:
Знать, от века божья кара
Ей назначена в удел.
И сбылося: что не смели
Снять мечи — взяла чума!
И в одной могиле стлели
Шлем с оливой и чалма.
"""

import time
import threading
from .singleton import Singleton
from .i18n import I18n
import logging


class GenericServer:
    server_id = "UNSET"
    server_name = "RENAME_ME"
    server_interval = 2
    sleep_interval = 1

    start_after = []

    def __init__(self):
        self._server_last = 0
        self._is_stopped = False
        self._thread = None

        GenericServerHost().register_server(self)

    def step(self):
        raise ImportError(I18n()('general/inheritor/must-redeclarate', method='step', parent=self.__class__.__name__))

    def finalize(self):
        pass

    def serve_ever(self):
        while not self._is_stopped:
            while time.time() - self._server_last < self.server_interval and not self._is_stopped:
                time.sleep(self.sleep_interval)

            if not self._is_stopped:
                self.step()
                self._server_last = time.time()

        self.finalize()

    def start(self):
        self._is_stopped = False
        self._thread = threading.Thread(target=self.serve_ever)
        self._thread.start()

    def stop(self):
        self._is_stopped = True
        if self._thread and self._thread.is_alive():
            self._thread.join()

    def is_alive(self):
        if not self._thread:
            return False
        else:
            return self._thread.is_alive()


class GenericWorker:
    server_id = "UNKNOWN"
    server_name = "NOT_SET"

    start_after = []

    def __init__(self):
        GenericServerHost().register_server(self)

    def start(self):
        raise ImportError(I18n()('general/inheritor/must-redeclarate', method='start', parent=self.__class__.__name__))

    def stop(self):
        raise ImportError(I18n()('general/inheritor/must-redeclarate', method='stop', parent=self.__class__.__name__))

    # noinspection PyMethodMayBeStatic
    def is_alive(self):
        return True


# noinspection PyBroadException,PyBroadException,PyBroadException,PyBroadException
class GenericServerHost(metaclass=Singleton):
    def __init__(self):
        self.servers = {}

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def register_server(self, instance):
        try:
            self.servers[instance.server_id] = instance
        except:
            self.logger.exception('demeter/generic-server/server-host-register-error')

    def build_node(self, node, order, left, cycletest):
        if node not in self.servers:
            self.logger.debug('demeter/generic-server/unknown-dependency', {'dep': node})
            return False

        if node in order:
            return True

        if node in cycletest:
            self.logger.debug('demeter/generic-server/cycle-dependency', {'dep': node})
            return False

        cycletest.append(node)
        for key in self.servers[node].start_after:
            if not self.build_node(key, order, left, cycletest):
                self.logger.debug('demeter/generic-server/cycle-dependency', {'dep': key, 'lead': node})
                return False

        order.append(node)
        cycletest.pop()
        return True

    def startup(self):
        order = []
        left = []
        cycletest = []

        for key in self.servers:
            left.append(key)

        for key in left:
            self.build_node(key, order, left, cycletest)

        for key in order:
            self.start(key)

    def shutdown(self):
        for key in self.servers:
            self.stop(key)

    def start(self, server):
        if server not in self.servers:
            self.logger.info("demeter/generic-server/unknown-server", {'name': server})
            return False
        else:
            try:
                self.servers[server].start()
                self.logger.debug("demeter/generic-server/server-started", {'name': self.servers[server].server_name})
                return True
            except:
                logging.exception("")
                return False

    def stop(self, server):
        if server not in self.servers:
            self.logger.info("demeter/generic-server/unknown-server", {'name': server})
            return False
        else:
            try:
                self.servers[server].stop()
                self.logger.debug("demeter/generic-server/server-stopped", {'name': self.servers[server].server_name})
                return True
            except:
                return False

    def status(self, server):
        if server not in self.servers:
            self.logger.info("demeter/generic-server/unknown-server", {'name': server})
            return False
        else:
            try:
                return self.servers[server].is_alive()
            except:
                return False