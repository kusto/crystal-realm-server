__author__ = 'crystal'

from .chain import ChainProcessorManager


class GenericEncoder:
    name = "GenericCompressor"
    id_prefix = 'encoder/generic/'

    def __init__(self):
        ChainProcessorManager().build_processor(self.id_prefix + "encode", self.encode)
        ChainProcessorManager().build_processor(self.id_prefix + "decode", self.decode)

    def encode(self, data):
        pass

    def decode(self, data):
        pass


class IdleEncoder(GenericEncoder):
    name = "IdleEncoder"
    id_prefix = 'encoder/idle/'

    def encode(self, data):
        return data

    def decode(self, data):
        return data


IdleEncoder()


class UTF8Encoder(GenericEncoder):
    name = "UTF8Encoder"
    id_prefix = 'encoder/utf8/'

    def encode(self, data):
        if isinstance(data, str):
            return data.encode('utf-8')
        else:
            return data

    def decode(self, data):
        if isinstance(data, bytes):
            return data.decode('utf-8')
        else:
            return data


UTF8Encoder()

try:
    # noinspection PyUnresolvedReferences
    import base64

    class Base64Encoder(GenericEncoder):
        name = "Base64Encoder"
        id_prefix = 'encoder/base64/'

        def encode(self, data):
            return base64.b64encode(data)

        def decode(self, data):
            return base64.b64decode(data)

    Base64Encoder()

    class Base32Encoder(GenericEncoder):
        name = "Base32Encoder"
        id_prefix = 'encoder/base32/'

        def encode(self, data):
            return base64.b32encode(data)

        def decode(self, data):
            return base64.b32decode(data)

    Base32Encoder()

    class Base16Encoder(GenericEncoder):
        name = "Base16Encoder"
        id_prefix = 'encoder/base16/'

        def encode(self, data):
            return base64.b16encode(data)

        def decode(self, data):
            return base64.b16decode(data)

    Base16Encoder()

    class Ascii85Encoder(GenericEncoder):
        name = "Ascii85Encoder"
        id_prefix = 'encoder/ascii85/'

        def encode(self, data):
            return base64.a85encode(data)

        def decode(self, data):
            return base64.a85decode(data)

    Ascii85Encoder()

except ImportError:
    pass