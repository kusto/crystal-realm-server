__author__ = 'crystal'

import functools
import logging
# noinspection PyUnresolvedReferences
from .i18n import I18n


def deprecated(func):
    @functools.wraps(func)
    def new_func(*args, **kwargs):
        logging.getLogger(__name__).warning("demeter/decorator/deprecated-call", {'func': func.__name__})
        return func(*args, **kwargs)

    return new_func
