__author__ = 'crystal'

import logging
from .singleton import Singleton


class Chain:
    def __init__(self):
        self.chain = []

    def append(self, element):
        self.chain.append(element)

    def execute(self, inp):
        output = inp
        for processor in self.chain:
            if output:
                output = processor.process(output)
            else:
                return output

        return output


# noinspection PyBroadException
class ChainProcessor:
    def __init__(self):
        self.call = self.__class__.idle

    def set_call(self, call):
        self.call = call

        return self

    def process(self, inp):
        try:
            safe = self.call(inp)
            return safe
        except:
            logging.getLogger(__name__ + '.' + self.__class__.__name__).exception("general/error")
            return inp

    @staticmethod
    def idle(inp):
        return inp


# noinspection PyMethodMayBeStatic
class ChainProcessorManager(metaclass=Singleton):
    def __init__(self):
        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)
        self.processors = {}

    def register_processor(self, name, processor):
        if name in self.processors:
            self.logger.info("demeter/chain/processor-double-register", {'name': name})
            return False

        self.processors[name] = processor

        return True

    def build_processor(self, name, call):
        processor = ChainProcessor()
        processor.set_call(call)

        return self.register_processor(name, processor)

    def get_processor(self, name):
        if name in self.processors:
            return self.processors[name]
        else:
            self.logger.warning("demeter/chain/processor-404", {'name': name})
            return self.get_fallback_processor()

    def get_fallback_processor(self):
        return ChainProcessor()

    def build_chain(self, chain):
        ret = Chain()

        for name in chain:
            ret.append(self.get_processor(name))

        return ret