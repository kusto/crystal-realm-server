import json
import os
import logging
import string

from core.demeter.singleton import Singleton
from .storage import RecursiveDictStorage


class FormatDict(dict):
    # noinspection PyMethodMayBeStatic
    def __missing__(self, key):
        return "{" + key + "}"


class I18n(RecursiveDictStorage, metaclass=Singleton):
    separator = "/"
    default_value = "LANG_NOT_FOUND"

    PATH = "./app/i18n"

    def _(self, fstr, *args, **kwargs):
        fstr = self.get(fstr)
        fmt = string.Formatter()
        mapping = FormatDict(kwargs)
        return fmt.vformat(fstr, args, mapping)

    def __init__(self):
        super().__init__()

        self.lang = {}
        self._new = {}

        self.set_storage(self.lang)

        self.step()

    def __call__(self, stri, *args, **kwargs):
        return self._(stri, *args, **kwargs)

    def step(self):
        self._new = {}

        for d, dirs, files in os.walk(self.PATH):
            for f in files:
                path = os.path.join(d, f)
                data = I18n._load_file(path)
                self._new = I18n._merge(self._new, data)

        self.storage = self._new

    @staticmethod
    def _load_file(filename):
        data = {}
        try:
            data_file = open(filename)
            data = json.load(data_file)
        except Exception as e:
            logging.warning("Ошибка загрузка файла локализации " + filename + ": " + e.__str__())

        return data

    @staticmethod
    def _merge(a, b, path=None):
        if path is None: 
            path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    I18n._merge(a[key], b[key], path + [str(key)])
                elif a[key] == b[key]:
                    pass
                else:
                    logging.error("Файлы интернационализации несогласованы!")
            else:
                a[key] = b[key]
        return a
