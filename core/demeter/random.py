__author__ = 'crystal'

from .singleton import Singleton


class RandomProviderManager(metaclass=Singleton):
    def __init__(self):
        self._provider = DummyRandomProvider()

    def set_provider(self, provider):
        self._provider = provider

    def get_provider(self):
        return self._provider


class GenericRandomProvider:
    def __init__(self):
        pass

    def get_string(self, length=0):
        raise ImportError()

    def get_int(self, minv=0, maxv=100000):
        raise ImportError()


class DummyRandomProvider(GenericRandomProvider):
    def get_int(self, minv=0, maxv=100000):
        return 1

    def get_string(self, length=10):
        s = ""
        for i in range(0, length):
            s += "a"

        return s


try:
    from random import randrange

    class RandRandomProvider(GenericRandomProvider):

        def get_int(self, minv=0, maxv=100000):
            return randrange(minv, maxv)

        def get_string(self, length=10):
            s = ""
            chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"
            for i in range(0, length):
                s += chars[self.get_int(0, len(chars) - 1)]

            return s

    RandomProviderManager().set_provider(RandRandomProvider())
except ImportError:
    randrange = None
else:
    try:
        from crypt import mksalt

        class CryptRandomProvider(RandRandomProvider):

            def get_string(self, length=10):
                s = ""
                while len(s) < length:
                    s += mksalt()

                return s[:length]

        RandomProviderManager().set_provider(CryptRandomProvider())
    except ImportError:
        mksalt = None

try:
    from Crypto.Random.random import randint

    class CryptoRandomProvider(GenericRandomProvider):

        def get_int(self, minv=0, maxv=100000):
            return randint(minv, maxv)

        def get_string(self, length=10):
            s = ""
            chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"
            for i in range(0, length):
                s += chars[self.get_int(0, len(chars) - 1)]

            return s

    RandomProviderManager().set_provider(CryptoRandomProvider())
except ImportError:
    randint = None