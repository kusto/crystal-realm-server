import time
from .storage import DictStorage
from .random import RandomProviderManager


class TokenManager:
    def __init__(self, tokenclass):
        self.tokens = {}
        self.tokenclass = tokenclass

    def generate_string(self):
        return RandomProviderManager().get_provider() \
            .get_string(25)

    def create_token(self, *args, **kwargs):
        string = self.generate_string()
        while string in self.tokens:
            string = self.generate_string()

        self.tokens[string] = self.tokenclass(string, *args, **kwargs)

        return string

    def validate_token(self, token, *args, **kwargs):
        if token not in self.tokens:
            return False

        if self.tokens[token].validate(*args, **kwargs):
            self.tokens[token].refresh(*args, **kwargs)
            return True
        else:
            return False


class Token(DictStorage):
    storage = {}

    def __init__(self, uniq, *args, **kwargs):
        super().__init__()
        self._id = uniq

    def refresh(self, *args, **kwargs):
        pass

    def validate(self, *args, **kwargs):
        return True


class TimeoutToken(Token):
    def __init__(self, uniq, timeout=0, *args, **kwargs):
        super().__init__(uniq, *args, **kwargs)
        self._last = time.time()
        self._timeout = timeout

    def refresh(self, *args, **kwargs):
        self._last = time.time()

    def validate(self, *args, **kwargs):
        if 0 < self._timeout < time.time() - self._last:
            return False

        return True


class SignedToken(Token):
    def __init__(self, uniq, secret="", *args, **kwargs):
        super().__init__(uniq, *args, **kwargs)
        self._secret = crypt.crypt(secret, uniq)

    def refresh(self, *args, **kwargs):
        pass

    def validate(self, signature, *args, **kwargs):
        if self._secret == crypt.crypt(signature, self._id):
            return True

        return False