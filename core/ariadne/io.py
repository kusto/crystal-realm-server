__author__ = 'crystal'

import json
import logging
from core.pandora.pandora import Pandora
import core.demeter.chain
import core.demeter.compress
import core.demeter.encode
import core.demeter.crypt


# noinspection PyBroadException
class AriadneJSONEncoder(core.demeter.encode.GenericEncoder):
    name = "AriadneJSON"
    id_prefix = 'encoder/ariadne/json/'

    def encode(self, answer):
        if answer is None or answer == "":
            return ""
        if isinstance(answer, dict):
            string = json.dumps(answer)
        else:
            if isinstance(answer, list):
                string = json.dumps(answer)
            else:
                if isinstance(answer, str):
                    string = json.dumps({'type': 'system_message', 'msg': answer})
                else:
                    logging.getLogger(__name__) \
                        .info("ariadne/io-bad-answer-type", {'type': type(answer).__name__})
                    return ""
        # logging.debug(string)
        return string

    def decode(self, command):
        try:
            parsed = json.loads(command)
            return parsed
        except:
            logging.getLogger(__name__) \
                .exception("ariadne/io-bad-package", {'cmd': command})
            return {}


AriadneJSONEncoder()


class AriadneFormatingEncoder(core.demeter.encode.GenericEncoder):
    name = "AriadneJSON"
    id_prefix = 'encoder/ariadne/formating/'

    def encode(self, answer):
        if isinstance(answer, str):
            answer += "\n"
            return answer
        else:
            if isinstance(answer, bytes):
                return self.encode(answer.decode('utf-8'))
            else:
                return answer

    def decode(self, command):
        return command


AriadneFormatingEncoder()


# noinspection PyBroadException
class Input:
    @classmethod
    def parse(cls, command):
        chain = core.demeter.chain.ChainProcessorManager() \
            .build_chain(Pandora().box['ariadne']['io']['input'])
        return chain.execute(command)


class Output:
    @classmethod
    def format(cls, answer):
        chain = core.demeter.chain.ChainProcessorManager() \
            .build_chain(Pandora().box['ariadne']['io']['output'])
        z = chain.execute(answer)
        return z