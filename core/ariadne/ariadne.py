__author__ = 'crystal'

"""
Опамятуйся, Ариадна!
Я даровал тебе слух, я даровал тебе свой слух –
услышь же меня! –
Ежели не возненавидишь себя, себя не полюбишь.
Я – твой лабиринт...
"""

import logging

from . import io
import core.demeter.singleton


# noinspection PyBroadException
class Ariadne(metaclass=core.demeter.singleton.Singleton):
    def __init__(self):
        self.binds = {}
        self.cerberus = None

        self.logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

    def enable_cerberus(self, cerberus):
        self.cerberus = cerberus

    def bind(self, bind, function):
        self.binds[bind] = function

    def run(self, command, client=None):
        if command:

            packet = io.Input.parse(command)
            if 'type' in packet and self.cerberus:
                if not self.cerberus.can(packet, packet['type']):
                    return ""

                session = self.cerberus.sessions.get(packet)
                packet['session'] = session
                if session:
                    session['client'] = client

            # logging.debug(packet)
            try:
                function = self.binds[packet['type']]
                data = function(packet)

                if data and isinstance(data, dict) and ('type' not in data) and 'type' in packet:
                    data['type'] = packet['type']

                return io.Output.format(data)
            except:
                self.logger.exception("ariadne/ariadne-handler-fail", {'cmd': command})
                return ""
        else:
            return ""