#!/usr/bin/python3

"""
Груда поверженных изваяний, где жарит солнце,
Где не дает мертвое дерево тени, сверчок - утешения,
Высохший камень - плеска воды. Там есть
Только тень от багровой скалы
(Встань под тень этой багровой скалы),
И я покажу тебе то, чего ты не видел доселе,
Нечто, совсем не похожее на твою тень,
Что за тобою шагает утром,
Или на тень твою вечером, что встает пред тобою;
Я покажу тебе страх в горстке праха.
"""
import time
import logging
import core.demeter.loader

Loader = core.demeter.loader.Loader()

Loader.load()
Loader.start()
while True:
    try:
        time.sleep(1)
    except KeyboardInterrupt:
        logging.warning("Получена комбинация Ctrl+C, выключаем сервер")
        Loader.stop()
        exit(0)